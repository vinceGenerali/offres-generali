<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201029123555 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto ADD produit VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE chien_chat ADD produit VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE emprunteur ADD produit VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE habitation ADD produit VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE sante_senior ADD produit VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto DROP produit');
        $this->addSql('ALTER TABLE chien_chat DROP produit');
        $this->addSql('ALTER TABLE emprunteur DROP produit');
        $this->addSql('ALTER TABLE habitation DROP produit');
        $this->addSql('ALTER TABLE sante_senior DROP produit');
    }
}
