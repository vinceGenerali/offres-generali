<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201106142217 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gpn ADD code_campagne VARCHAR(255) NOT NULL, ADD url_referrer VARCHAR(255) NOT NULL, ADD tracking VARCHAR(255) NOT NULL, ADD produit VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE retraite ADD code_campagne VARCHAR(255) NOT NULL, ADD url_referrer VARCHAR(255) NOT NULL, ADD tracking VARCHAR(255) NOT NULL, ADD produit VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gpn DROP code_campagne, DROP url_referrer, DROP tracking, DROP produit');
        $this->addSql('ALTER TABLE retraite DROP code_campagne, DROP url_referrer, DROP tracking, DROP produit');
    }
}
