<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201113103255 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ideo_sante CHANGE date_naissance date_naissance VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE retraite CHANGE date_naissance date_naissance VARCHAR(10) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ideo_sante CHANGE date_naissance date_naissance DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE retraite CHANGE date_naissance date_naissance DATETIME NOT NULL');
    }
}
