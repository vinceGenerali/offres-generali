<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201027172551 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto CHANGE date_mise_en_circulation date_mise_en_circulation DATETIME DEFAULT NULL, CHANGE dateobtention_permis dateobtention_permis DATETIME DEFAULT NULL, CHANGE lieu_garage lieu_garage VARCHAR(255) DEFAULT NULL, CHANGE cas_utilisation_vehicule cas_utilisation_vehicule VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE chien_chat CHANGE date_naissance_animal date_naissance_animal DATETIME DEFAULT NULL, CHANGE identification_animal identification_animal VARCHAR(255) DEFAULT NULL, CHANGE race race VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE gpn DROP date_rappel_souhaitee, CHANGE raison_sociale raison_sociale VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE habitation CHANGE statut_occupant statut_occupant VARCHAR(255) DEFAULT NULL, CHANGE type_habitation type_habitation VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE ideo_sante CHANGE raison_sociale raison_sociale VARCHAR(255) DEFAULT NULL, CHANGE date_naissance date_naissance DATETIME DEFAULT NULL, CHANGE statut statut VARCHAR(255) DEFAULT NULL, CHANGE regime regime VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE mrc CHANGE statut_occupant statut_occupant VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE retraite CHANGE raison_sociale raison_sociale VARCHAR(255) DEFAULT NULL, CHANGE statut statut VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE sante_senior CHANGE regime regime VARCHAR(255) DEFAULT NULL, CHANGE beneficiaire beneficiaire VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto CHANGE date_mise_en_circulation date_mise_en_circulation DATETIME NOT NULL, CHANGE dateobtention_permis dateobtention_permis DATETIME NOT NULL, CHANGE lieu_garage lieu_garage LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE cas_utilisation_vehicule cas_utilisation_vehicule LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE chien_chat CHANGE date_naissance_animal date_naissance_animal DATETIME NOT NULL, CHANGE identification_animal identification_animal LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE race race LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE gpn ADD date_rappel_souhaitee DATETIME NOT NULL, CHANGE raison_sociale raison_sociale VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE habitation CHANGE statut_occupant statut_occupant VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE type_habitation type_habitation VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE ideo_sante CHANGE raison_sociale raison_sociale VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE date_naissance date_naissance DATETIME NOT NULL, CHANGE statut statut VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE regime regime VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE mrc CHANGE statut_occupant statut_occupant VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE retraite CHANGE raison_sociale raison_sociale VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE statut statut VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE sante_senior CHANGE regime regime VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE beneficiaire beneficiaire VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
