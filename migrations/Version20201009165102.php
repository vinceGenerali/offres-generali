<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201009165102 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE argument_produit ADD id_formulaire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE argument_produit ADD CONSTRAINT FK_5653F5B5EE7B87FE FOREIGN KEY (id_formulaire_id) REFERENCES liste_forms (id)');
        $this->addSql('CREATE INDEX IDX_5653F5B5EE7B87FE ON argument_produit (id_formulaire_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE argument_produit DROP FOREIGN KEY FK_5653F5B5EE7B87FE');
        $this->addSql('DROP INDEX IDX_5653F5B5EE7B87FE ON argument_produit');
        $this->addSql('ALTER TABLE argument_produit DROP id_formulaire_id');
    }
}
