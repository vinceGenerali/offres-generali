<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201014140413 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mrc ADD civilite VARCHAR(255) NOT NULL, ADD nom VARCHAR(255) NOT NULL, ADD prenom VARCHAR(255) NOT NULL, ADD telephone VARCHAR(10) NOT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD adresse LONGTEXT NOT NULL, ADD cplt_adresse LONGTEXT DEFAULT NULL, ADD cp VARCHAR(5) NOT NULL, ADD ville VARCHAR(255) NOT NULL, ADD activite VARCHAR(255) DEFAULT NULL, ADD nombre_salarie INT DEFAULT NULL, ADD ca INT DEFAULT NULL, ADD superficie INT DEFAULT NULL, ADD valeur_mobilier INT DEFAULT NULL, ADD statut_occupant VARCHAR(255) NOT NULL, ADD date_rappel_souhaitee VARCHAR(255) NOT NULL, ADD date DATETIME NOT NULL, ADD adresse_ip VARCHAR(16) DEFAULT NULL, ADD optin_generali VARCHAR(5) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mrc DROP civilite, DROP nom, DROP prenom, DROP telephone, DROP email, DROP adresse, DROP cplt_adresse, DROP cp, DROP ville, DROP activite, DROP nombre_salarie, DROP ca, DROP superficie, DROP valeur_mobilier, DROP statut_occupant, DROP date_rappel_souhaitee, DROP date, DROP adresse_ip, DROP optin_generali');
    }
}
