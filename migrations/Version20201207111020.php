<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201207111020 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto CHANGE url_referrer url_referrer LONGTEXT DEFAULT NULL, CHANGE tracking tracking LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chien_chat CHANGE url_referrer url_referrer LONGTEXT DEFAULT NULL, CHANGE tracking tracking LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE emprunteur CHANGE url_referrer url_referrer LONGTEXT DEFAULT NULL, CHANGE tracking tracking LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE gpn CHANGE url_referrer url_referrer LONGTEXT DEFAULT NULL, CHANGE tracking tracking LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE habitation CHANGE url_referrer url_referrer LONGTEXT DEFAULT NULL, CHANGE tracking tracking LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE ideo_sante CHANGE url_referrer url_referrer LONGTEXT DEFAULT NULL, CHANGE tracking tracking LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE mrc CHANGE ca ca BIGINT DEFAULT NULL, CHANGE url_referrer url_referrer LONGTEXT DEFAULT NULL, CHANGE tracking tracking LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE prevoyance CHANGE raison_sociale raison_sociale VARCHAR(255) DEFAULT NULL, CHANGE statut statut VARCHAR(255) DEFAULT NULL, CHANGE regime regime VARCHAR(255) DEFAULT NULL, CHANGE rente_conjoint rente_conjoint VARCHAR(255) DEFAULT NULL, CHANGE rente_etudiant rente_etudiant VARCHAR(255) DEFAULT NULL, CHANGE url_referrer url_referrer LONGTEXT DEFAULT NULL, CHANGE tracking tracking LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE retraite CHANGE url_referrer url_referrer LONGTEXT DEFAULT NULL, CHANGE tracking tracking LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE sante_senior CHANGE url_referrer url_referrer LONGTEXT DEFAULT NULL, CHANGE tracking tracking LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto CHANGE url_referrer url_referrer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE tracking tracking VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE chien_chat CHANGE url_referrer url_referrer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE tracking tracking VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE emprunteur CHANGE url_referrer url_referrer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE tracking tracking VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE gpn CHANGE url_referrer url_referrer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE tracking tracking VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE habitation CHANGE url_referrer url_referrer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE tracking tracking VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE ideo_sante CHANGE url_referrer url_referrer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE tracking tracking VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE mrc CHANGE ca ca INT DEFAULT NULL, CHANGE url_referrer url_referrer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE tracking tracking VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE prevoyance CHANGE raison_sociale raison_sociale VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE statut statut VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE regime regime VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE rente_conjoint rente_conjoint TINYINT(1) NOT NULL, CHANGE rente_etudiant rente_etudiant TINYINT(1) NOT NULL, CHANGE url_referrer url_referrer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE tracking tracking VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE retraite CHANGE url_referrer url_referrer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE tracking tracking VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE sante_senior CHANGE url_referrer url_referrer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE tracking tracking VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
