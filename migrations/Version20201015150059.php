<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201015150059 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE prevoyance ADD civilite VARCHAR(255) NOT NULL, ADD nom VARCHAR(255) NOT NULL, ADD prenom VARCHAR(255) NOT NULL, ADD telephone VARCHAR(10) NOT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD adresse LONGTEXT NOT NULL, ADD cplt_adresse LONGTEXT DEFAULT NULL, ADD cp VARCHAR(5) NOT NULL, ADD ville VARCHAR(255) NOT NULL, ADD raison_sociale VARCHAR(255) NOT NULL, ADD statut VARCHAR(255) NOT NULL, ADD regime VARCHAR(255) NOT NULL, ADD activite VARCHAR(255) DEFAULT NULL, ADD type_assurance_recherche VARCHAR(255) DEFAULT NULL, ADD rente_conjoint TINYINT(1) NOT NULL, ADD rente_etudiant TINYINT(1) NOT NULL, ADD date_rappel_souhaitee DATETIME NOT NULL, ADD date DATETIME NOT NULL, ADD adresse_ip VARCHAR(16) DEFAULT NULL, ADD optin_generali VARCHAR(5) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE prevoyance DROP civilite, DROP nom, DROP prenom, DROP telephone, DROP email, DROP adresse, DROP cplt_adresse, DROP cp, DROP ville, DROP raison_sociale, DROP statut, DROP regime, DROP activite, DROP type_assurance_recherche, DROP rente_conjoint, DROP rente_etudiant, DROP date_rappel_souhaitee, DROP date, DROP adresse_ip, DROP optin_generali');
    }
}
