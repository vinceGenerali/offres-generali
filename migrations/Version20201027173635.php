<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201027173635 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto DROP date_rappel_souhaitee');
        $this->addSql('ALTER TABLE chien_chat DROP date_rappel_souhaitee');
        $this->addSql('ALTER TABLE emprunteur DROP date_rappel_souhaitee');
        $this->addSql('ALTER TABLE habitation DROP date_rappel_souhaitee');
        $this->addSql('ALTER TABLE ideo_sante DROP date_rappel_souhaitee');
        $this->addSql('ALTER TABLE mrc DROP date_rappel_souhaitee');
        $this->addSql('ALTER TABLE prevoyance DROP date_rappel_souhaitee');
        $this->addSql('ALTER TABLE retraite DROP date_rappel_souhaitee');
        $this->addSql('ALTER TABLE sante_senior DROP date_rappel_souhaitee');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto ADD date_rappel_souhaitee DATETIME NOT NULL');
        $this->addSql('ALTER TABLE chien_chat ADD date_rappel_souhaitee DATETIME NOT NULL');
        $this->addSql('ALTER TABLE emprunteur ADD date_rappel_souhaitee DATETIME NOT NULL');
        $this->addSql('ALTER TABLE habitation ADD date_rappel_souhaitee DATETIME NOT NULL');
        $this->addSql('ALTER TABLE ideo_sante ADD date_rappel_souhaitee DATETIME NOT NULL');
        $this->addSql('ALTER TABLE mrc ADD date_rappel_souhaitee DATETIME NOT NULL');
        $this->addSql('ALTER TABLE prevoyance ADD date_rappel_souhaitee DATETIME NOT NULL');
        $this->addSql('ALTER TABLE retraite ADD date_rappel_souhaitee DATETIME NOT NULL');
        $this->addSql('ALTER TABLE sante_senior ADD date_rappel_souhaitee DATETIME NOT NULL');
    }
}
