<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201015153148 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE auto (id INT AUTO_INCREMENT NOT NULL, civilite VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, telephone VARCHAR(10) NOT NULL, email VARCHAR(255) DEFAULT NULL, adresse LONGTEXT NOT NULL, cplt_adresse LONGTEXT DEFAULT NULL, cp VARCHAR(5) NOT NULL, ville VARCHAR(255) NOT NULL, date_mise_en_circulation DATETIME NOT NULL, dateobtention_permis DATETIME NOT NULL, lieu_garage LONGTEXT NOT NULL, cas_utilisation_vehicule LONGTEXT NOT NULL, date_effet_contrat DATETIME NOT NULL, date_rappel_souhaitee DATETIME NOT NULL, date DATETIME NOT NULL, adresse_ip VARCHAR(16) DEFAULT NULL, optin_generali VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE auto');
    }
}
