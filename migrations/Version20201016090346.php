<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201016090346 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sante_senior ADD civilite VARCHAR(255) NOT NULL, ADD nom VARCHAR(255) NOT NULL, ADD prenom VARCHAR(255) NOT NULL, ADD telephone VARCHAR(10) NOT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD adresse LONGTEXT NOT NULL, ADD cplt_adresse LONGTEXT DEFAULT NULL, ADD cp VARCHAR(5) NOT NULL, ADD ville VARCHAR(255) NOT NULL, ADD date_naissance DATETIME NOT NULL, ADD regime VARCHAR(255) NOT NULL, ADD beneficiaire VARCHAR(255) NOT NULL, ADD date_rappel_souhaitee DATETIME NOT NULL, ADD date DATETIME NOT NULL, ADD adresse_ip VARCHAR(16) DEFAULT NULL, ADD optin_generali VARCHAR(5) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sante_senior DROP civilite, DROP nom, DROP prenom, DROP telephone, DROP email, DROP adresse, DROP cplt_adresse, DROP cp, DROP ville, DROP date_naissance, DROP regime, DROP beneficiaire, DROP date_rappel_souhaitee, DROP date, DROP adresse_ip, DROP optin_generali');
    }
}
