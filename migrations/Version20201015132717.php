<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201015132717 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE retraite (id INT AUTO_INCREMENT NOT NULL, civilite VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, telephone VARCHAR(10) NOT NULL, email VARCHAR(255) DEFAULT NULL, adresse LONGTEXT NOT NULL, cplt_adresse LONGTEXT DEFAULT NULL, cp VARCHAR(5) NOT NULL, ville VARCHAR(255) NOT NULL, raison_sociale VARCHAR(255) NOT NULL, date_naissance DATETIME NOT NULL, statut VARCHAR(255) NOT NULL, activite VARCHAR(255) DEFAULT NULL, souhait_age_retraite INT DEFAULT NULL, capacite_epargne INT DEFAULT NULL, date_rappel_souhaitee DATETIME NOT NULL, date DATETIME NOT NULL, adresse_ip VARCHAR(16) DEFAULT NULL, optin_generali VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE retraite');
    }
}
