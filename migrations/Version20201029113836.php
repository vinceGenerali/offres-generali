<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201029113836 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto DROP cplt_adresse, CHANGE date_mise_en_circulation date_mise_en_circulation VARCHAR(10) DEFAULT NULL, CHANGE dateobtention_permis dateobtention_permis VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE chien_chat DROP cplt_adresse');
        $this->addSql('ALTER TABLE emprunteur DROP cplt_adresse');
        $this->addSql('ALTER TABLE gpn DROP cplt_adresse');
        $this->addSql('ALTER TABLE habitation DROP cplt_adresse');
        $this->addSql('ALTER TABLE ideo_sante DROP cplt_adresse');
        $this->addSql('ALTER TABLE mrc DROP cplt_adresse');
        $this->addSql('ALTER TABLE prevoyance DROP cplt_adresse');
        $this->addSql('ALTER TABLE retraite DROP cplt_adresse');
        $this->addSql('ALTER TABLE sante_senior DROP cplt_adresse');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto ADD cplt_adresse LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE date_mise_en_circulation date_mise_en_circulation DATE DEFAULT NULL, CHANGE dateobtention_permis dateobtention_permis DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE chien_chat ADD cplt_adresse LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE emprunteur ADD cplt_adresse LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE gpn ADD cplt_adresse LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE habitation ADD cplt_adresse LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE ideo_sante ADD cplt_adresse LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE mrc ADD cplt_adresse LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE prevoyance ADD cplt_adresse LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE retraite ADD cplt_adresse LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE sante_senior ADD cplt_adresse LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
