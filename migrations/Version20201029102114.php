<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201029102114 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto ADD code_campagne VARCHAR(255) NOT NULL, ADD url_referrer VARCHAR(255) NOT NULL, ADD tracking VARCHAR(255) NOT NULL, CHANGE lieu_garage lieu_garage VARCHAR(5) DEFAULT NULL, CHANGE date_effet_contrat date_effet_contrat VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE chien_chat ADD code_campagne VARCHAR(255) NOT NULL, ADD url_referrer VARCHAR(255) NOT NULL, ADD tracking VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE emprunteur ADD code_campagne VARCHAR(255) NOT NULL, ADD url_referrer VARCHAR(255) NOT NULL, ADD tracking VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE habitation ADD code_campagne VARCHAR(255) NOT NULL, ADD url_referrer VARCHAR(255) NOT NULL, ADD tracking VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE sante_senior ADD code_campagne VARCHAR(255) NOT NULL, ADD url_referrer VARCHAR(255) NOT NULL, ADD tracking VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto DROP code_campagne, DROP url_referrer, DROP tracking, CHANGE lieu_garage lieu_garage VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE date_effet_contrat date_effet_contrat DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE chien_chat DROP code_campagne, DROP url_referrer, DROP tracking');
        $this->addSql('ALTER TABLE emprunteur DROP code_campagne, DROP url_referrer, DROP tracking');
        $this->addSql('ALTER TABLE habitation DROP code_campagne, DROP url_referrer, DROP tracking');
        $this->addSql('ALTER TABLE sante_senior DROP code_campagne, DROP url_referrer, DROP tracking');
    }
}
