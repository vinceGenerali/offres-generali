<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201019103745 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto ADD source_client VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE chien_chat ADD source_client VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE emprunteur ADD source_client VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE gpn ADD source_client VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE habitation ADD source_client VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ideo_sante ADD source_client VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE mrc ADD source_client VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE prevoyance ADD source_client VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE retraite ADD source_client VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE sante_senior ADD source_client VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto DROP source_client');
        $this->addSql('ALTER TABLE chien_chat DROP source_client');
        $this->addSql('ALTER TABLE emprunteur DROP source_client');
        $this->addSql('ALTER TABLE gpn DROP source_client');
        $this->addSql('ALTER TABLE habitation DROP source_client');
        $this->addSql('ALTER TABLE ideo_sante DROP source_client');
        $this->addSql('ALTER TABLE mrc DROP source_client');
        $this->addSql('ALTER TABLE prevoyance DROP source_client');
        $this->addSql('ALTER TABLE retraite DROP source_client');
        $this->addSql('ALTER TABLE sante_senior DROP source_client');
    }
}
