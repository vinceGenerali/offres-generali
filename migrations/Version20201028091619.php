<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201028091619 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto CHANGE date_mise_en_circulation date_mise_en_circulation DATE DEFAULT NULL, CHANGE dateobtention_permis dateobtention_permis DATE DEFAULT NULL, CHANGE date_effet_contrat date_effet_contrat DATE DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auto CHANGE date_mise_en_circulation date_mise_en_circulation DATETIME DEFAULT NULL, CHANGE dateobtention_permis dateobtention_permis DATETIME DEFAULT NULL, CHANGE date_effet_contrat date_effet_contrat DATETIME NOT NULL');
    }
}
