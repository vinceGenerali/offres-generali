import $ from "jquery";
import 'inputmask/lib/jquery.inputmask';

$(document).ready(function () {
    /*************Initialisation iCheck **************/
   $(".datepicker-input, .birthdatepicker").inputmask({
        alias: "99/99/9999",
        placeholder: "jj/mm/yyyy",
        clearIncomplete: true
   });

    $(".codePostal-input").inputmask({
        alias: "99999",
        placeholder: "_____",
        clearIncomplete: true
    });

    /*************Personnalisation champ catégorie animale formulaire SPA *************
    $('#spa_typeAnimal').on('change', function() {
        if (this.value === 'Chien') {
            let modal = $('#modalCategorieAnimal');
            modal.modal('show')
            modal.on('shown.bs.modal', function () {
                $('#spa_categorieChien').trigger('focus')
            })
        }
    });*/

    /*************Personnalisation champ catégorie animale formulaire SPA **************/
    function diffDateLimit(dateInput) {
        let todayDate = new Date().getFullYear(); //Today Date
        let dateSaisi = new Date(dateInput.split('/').reverse().join('-')).getFullYear();
        let limit = 7;
        if (todayDate - dateSaisi > limit){
            let modal = $('#modalAgeAnimal')
            modal.modal('show')
            $('#btnFormSPA').attr(
                {
                    disabled: 'disabled',
                    'aria-disabled': true
                })
        }else{
            $('#btnFormSPA').removeAttr('disabled aria-disabled')
        }

    }
    $('#spa_dateNaissanceAnimal').on('blur', function() {
        let dateInput = this.value
        diffDateLimit(dateInput)
    });
    /**********************************************************************************/

    /*$('.datepicker-input').flatpickr(
        {
            "locale": "fr",  // locale for this instance only
            minDate: "today",
            maxDate: new Date().fp_incr(30), // 14 days from now
            altInput: true,
            altFormat: "j F Y",
            dateFormat: "d/m/Y",
        }
    );*/
});