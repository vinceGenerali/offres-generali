import { Controller } from '@hotwired/stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * checkbox_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    connect() {
        this.element.addEventListener('click', () => {
            let button = document.getElementById("btnSubmit");
            if (button.disabled === false)
                button.disabled = true;
            else
                button.disabled = false;
        });
    }
}
