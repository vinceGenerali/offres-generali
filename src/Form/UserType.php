<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', TextType::class, [
                "label" => 'Nom complet'
            ])
            ->add('username', TextType::class, [
                "label" => 'Nom affiché'
            ])
            ->add('email', TextType::class, [
                "label" => 'Email',
            ])
            ->add('password', TextType::class, [
                "label" => 'Mot de passe'
            ])
            ->add('roles', HiddenType::class, [
                'mapped' => false
            ])
            ->add('fonction', TextType::class, [
                "label" => 'Fonction'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
