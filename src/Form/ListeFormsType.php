<?php

namespace App\Form;

use App\Entity\ListeForms;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListeFormsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('NomFormulaire')
            ->add('NomProduit')
            ->add('UniversProduit')
            ->add('DateCreation')
            ->add('urlFormulaire')
            ->add('urlAdminFormulaire')
            ->add('visuelEntete')
            ->add('logoProduit')
            ->add('pictoMenu')
            ->add('campagne')
            ->add('tcFormName')
            ->add('tcProductCategory');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ListeForms::class,
        ]);
    }
}
