<?php

namespace App\Form;

use App\Entity\ArgumentProduit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArgumentProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('text')
            ->add('libelleLien')
            ->add('lien')
            ->add('picto')
            ->add('idFormulaire');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ArgumentProduit::class,
        ]);
    }
}
