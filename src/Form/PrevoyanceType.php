<?php

namespace App\Form;

use App\Entity\Prevoyance;
use App\Entity\RegimeSociale;
use App\Entity\Statut;
use App\Entity\TypeAssuranceRecherche;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrevoyanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('civilite', ChoiceType::class, [
                'choices' => [
                    'Madame' => 'Mme',
                    'Monsieur' => 'M.'
                ],
                'expanded' => false,
                'multiple' => false
            ])
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class, [
                "label" => 'Prénom'
            ])
            ->add('telephone', TelType::class, [
                'attr' => ['minlength' => 10, 'maxlength' => 10],
                "label" => 'Téléphone',
            ])
            ->add('email', EmailType::class, [
                "label" => 'Adresse e-mail'
            ])
            ->add('adresse', TextType::class, [
                "label" => 'Adresse postale'
            ])
            ->add('cp', TelType::class, [
                'attr' => ['minlength' => 5, 'maxlength' => 5],
                "label" => 'Code postal'
            ])
            ->add('ville', TextType::class)
            /*->add('ville', ChoiceType::class, [
                'expanded' => false,
                'multiple' => false,
            ])*/
            ->add('raisonSociale', TextType::class, [
                "label" => 'Raison sociale de l\'entreprise',
                'required' => false,
            ])
            ->add('statut', EntityType::class, [
                "label" => 'Statut',
                'class' => Statut::class,
                'choice_label' => 'valeur',
                // 'multiple' => true,
                // 'expanded' => true,
                'required' => false,
            ])
            ->add('regime', EntityType::class, [
                "label" => 'Régime social',
                'class' => RegimeSociale::class,
                'choice_label' => 'valeur',
                'required' => false,
                // 'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('activite', TextType::class, [
                "label" => 'Activité de l\'entreprise',
                'required' => false,
            ])
            ->add('typeAssuranceRecherche', EntityType::class, [
                "label" => 'Quel type d\'assurance recherchez-vous ?',
                'class' => TypeAssuranceRecherche::class,
                'choice_label' => 'valeur',
                // 'multiple' => true,
                // 'expanded' => true,
                'required' => false,
            ])
            ->add('renteConjoint', ChoiceType::class, [
                'choices' => [
                    'Oui' => 'Oui',
                    'Non' => 'Non'
                ],
                'expanded' => false,
                'multiple' => false,
                'required' => false,
            ])
            ->add('renteEtudiant', ChoiceType::class, [
                'choices' => [
                    'Oui' => 'Oui',
                    'Non' => 'Non'
                ],
                'expanded' => false,
                'multiple' => false,
                'required' => false,
            ])
            ->add('optinGenerali', CheckboxType::class, [
                'label' => 'J’accepte de recevoir par tout moyen de contact notamment par téléphone ou par voie électronique, de la part des compagnies du Groupe GENERALI, - Generali-VIE ou IARD, Europ Assistance, l’Equité - directement ou par son réseau d’intermédiaires, des informations et offres commerciales concernant des produits d’assurances et produits ou services accessoires.',
                'required' => false
            ])
            ->add('date', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('adresseIP', HiddenType::class, [
                'mapped' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Prevoyance::class,
        ]);
    }
}
