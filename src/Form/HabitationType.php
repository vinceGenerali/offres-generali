<?php

namespace App\Form;

use App\Entity\Habitation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HabitationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('civilite', ChoiceType::class, [
                'choices' => [
                    'Madame' => 'Madame',
                    'Monsieur' => 'Monsieur'
                ],
                'expanded' => false,
                'multiple' => false
            ])
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class, [
                "label" => 'Prénom'
            ])
            ->add('telephone', TelType::class, [
                'attr' => ['minlength' => 10, 'maxlength' => 10],
                "label" => 'Téléphone',
            ])
            ->add('email', EmailType::class, [
                "label" => 'Adresse e-mail'
            ])
            ->add('adresse', TextType::class, [
                "label" => 'Adresse postale'
            ])
            ->add('cp', TelType::class, [
                'attr' => ['minlength' => 5, 'maxlength' => 5],
                "label" => 'Code postal'
            ])
            ->add('ville', TextType::class)
            /*->add('ville', ChoiceType::class, [
                'expanded' => false,
                'multiple' => false,
            ])*/
            ->add('statutOccupant', ChoiceType::class, [
                'choices' => [
                    'Propriétaire' => 'Propriétaire',
                    'Locataire' => 'Locataire'
                ],
                'required' => false,
                'expanded' => false,
                'multiple' => false
            ])
            ->add('typeHabitation', ChoiceType::class, [
                'choices' => [
                    'Maison' => 'Maison',
                    'Appartement' => 'Appartement'
                ],
                'required' => false,
                'expanded' => false,
                'multiple' => false
            ])
            ->add('optinGenerali', CheckboxType::class, [
                'label' => 'J’accepte de recevoir par tout moyen de contact notamment par téléphone ou par voie électronique, de la part des compagnies du Groupe GENERALI, - Generali-VIE ou IARD, Europ Assistance, l’Equité - directement ou par son réseau d’intermédiaires, des informations et offres commerciales concernant des produits d’assurances et produits ou services accessoires.',
                'required' => false
            ])
            ->add('date', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('adresseIP', HiddenType::class, [
                'mapped' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Habitation::class,
            'csrf_protection' => true,
        ]);
    }
}
