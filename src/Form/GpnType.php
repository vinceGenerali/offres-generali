<?php

namespace App\Form;

use App\Entity\Gpn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GpnType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('civilite', ChoiceType::class, [
                'choices' => [
                    'Madame' => 'Mme',
                    'Monsieur' => 'M.'
                ],
                'expanded' => false,
                'multiple' => false
            ])
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class, [
                "label" => 'Prénom'
            ])
            ->add('telephone', TelType::class, [
                'attr' => ['minlength' => 10, 'maxlength' => 10],
                "label" => 'Téléphone',
            ])
            ->add('email', EmailType::class, [
                "label" => 'Adresse e-mail'
            ])
            ->add('fonction', TextType::class, [
                "label" => 'Votre poste',
                'required' => false,
            ])
            ->add('adresse', TextType::class, [
                "label" => 'Adresse postale'
            ])
            ->add('cp', TelType::class, [
                'attr' => ['minlength' => 5, 'maxlength' => 5],
                "label" => 'Code postal'
            ])
            ->add('ville', TextType::class)
            /*->add('ville', ChoiceType::class, [
                'expanded' => false,
                'multiple' => false,
            ])*/
            ->add('raisonSociale', TextType::class, [
                "label" => 'Raison sociale de l\'entreprise',
                'required' => false
            ])
            ->add('nombreSalarie', IntegerType::class, [
                'attr' => ['min' => 0],
                "label" => 'Nombre de salarié(s)',
                'required' => false,
            ])
            ->add('activite', TextType::class, [
                "label" => 'Activité de l\'entreprise',
                'required' => false,
            ])
            ->add('cA', IntegerType::class, [
                "label" => 'Chiffre d\'affaires',
                'required' => false,
                'attr' => ['min' => 0],
            ])
            ->add('eCommercant', ChoiceType::class, [
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ],
                'required' => false,
                'expanded' => false,
                'multiple' => false,
                'label' => 'Commercialisez-vous des produits et/ou des services sur internet ?'
            ])
            ->add('optinGenerali', CheckboxType::class, [
                'label' => 'J’accepte de recevoir par tout moyen de contact notamment par téléphone ou par voie électronique, de la part des compagnies du Groupe GENERALI, - Generali-VIE ou IARD, Europ Assistance, l’Equité - directement ou par son réseau d’intermédiaires, des informations et offres commerciales concernant des produits d’assurances et produits ou services accessoires.',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gpn::class,
            'csrf_protection' => true,
        ]);
    }
}
