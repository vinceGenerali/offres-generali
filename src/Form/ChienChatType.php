<?php

namespace App\Form;

use App\Entity\ChienChat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChienChatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('civilite', ChoiceType::class, [
                'choices' => [
                    'Madame' => 'Madame',
                    'Monsieur' => 'Monsieur'
                ],
                'expanded' => false,
                'multiple' => false
            ])
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class, [
                "label" => 'Prénom'
            ])
            ->add('telephone', TelType::class, [
                'attr' => ['minlength' => 10, 'maxlength' => 10],
                "label" => 'Téléphone',
            ])
            ->add('email', EmailType::class, [
                "label" => 'Adresse e-mail'
            ])
            ->add('adresse', TextType::class, [
                "label" => 'Adresse postale'
            ])
            ->add('cp', TelType::class, [
                'attr' => ['minlength' => 5, 'maxlength' => 5],
                "label" => 'Code postal'
            ])
            ->add('ville', TextType::class)
            /*->add('ville', ChoiceType::class, [
                'expanded' => false,
                'multiple' => false,
            ])*/
            ->add('typeAnimal', ChoiceType::class, [
                'choices' => [
                    'Chat' => 'Chat',
                    'Chien' => 'Chien'
                ],
                'expanded' => false,
                'multiple' => false,
                'required' => false,
                'label' => 'S’agit-il d’un chien ou un chat ? ',
            ])
            ->add('dateNaissanceAnimal', TextType::class, [
                "label" => 'Date de naissance de votre animal',
                "attr" => [
                    'class' => 'birthdatepicker',
                    'data-target' => '#datetimepicker',
                ],
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'required' => false
            ])
            ->add('identificationAnimal', ChoiceType::class, [
                'choices' => [
                    'Tatoué' => 'Tatoué',
                    'Pucé' => 'Pucé'
                ],
                'expanded' => false,
                'multiple' => false,
                'required' => false,
                'label' => 'Est-ce que votre animal est tatoué ou pucé ?',
            ])
            ->add('race', TextType::class, [
                'required' => false,
                "label" => 'Quelle est la race de votre animal de compagnie ?'
            ])
            ->add('optinGenerali', CheckboxType::class, [
                'label' => 'J’accepte de recevoir par tout moyen de contact notamment par téléphone ou par voie électronique, de la part des compagnies du Groupe GENERALI, - Generali-VIE ou IARD, Europ Assistance, l’Equité - directement ou par son réseau d’intermédiaires, des informations et offres commerciales concernant des produits d’assurances et produits ou services accessoires.',
                'required' => false
            ])
            ->add('date', HiddenType::class, [
                'mapped' => false,
            ])
            ->add('adresseIP', HiddenType::class, [
                'mapped' => false,
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ChienChat::class,
        ]);
    }
}
