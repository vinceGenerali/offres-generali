<?php

namespace App\Entity;

use App\Repository\ArgumentProduitRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArgumentProduitRepository::class)
 */
class ArgumentProduit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelleLien;

    /**
     * @return mixed
     */
    public function getLibelleLien()
    {
        return $this->libelleLien;
    }

    /**
     * @param mixed $libelleLien
     */
    public function setLibelleLien($libelleLien): void
    {
        $this->libelleLien = $libelleLien;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lien;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picto;

    /**
     * @ORM\ManyToOne(targetEntity=ListeForms::class, inversedBy="argumentProduits")
     */
    private $idFormulaire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getLien(): ?string
    {
        return $this->lien;
    }

    public function setLien(string $lien): self
    {
        $this->lien = $lien;

        return $this;
    }

    public function getPicto(): ?string
    {
        return $this->picto;
    }

    public function setPicto(string $picto): self
    {
        $this->picto = $picto;

        return $this;
    }

    public function getIdFormulaire(): ?ListeForms
    {
        return $this->idFormulaire;
    }

    public function setIdFormulaire(?ListeForms $idFormulaire): self
    {
        $this->idFormulaire = $idFormulaire;

        return $this;
    }
}
