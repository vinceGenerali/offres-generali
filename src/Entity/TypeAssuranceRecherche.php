<?php

namespace App\Entity;

use App\Repository\TypeAssuranceRechercheRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeAssuranceRechercheRepository::class)
 */
class TypeAssuranceRecherche
{
    //Fonction toString() permettant la traduction de l'objet en String

    public function __toString()
    {
        return $this->valeur;
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $valeur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValeur(): ?string
    {
        return $this->valeur;
    }

    public function setValeur(string $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }
}
