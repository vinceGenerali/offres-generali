<?php

namespace App\Entity;

use App\Repository\EmprunteurRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EmprunteurRepository::class)
 */
class Emprunteur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $civilite;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Nom est obligatoire")
     * @Assert\Regex(
     *     pattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     htmlPattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     message="Le champ Nom ne peut contenir que des lettres")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Prénom est obligatoire")
     * @Assert\Regex(
     *     pattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     htmlPattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     message="Le champ Prénom ne peut contenir que des lettres")
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank(message="Le champ Téléphone est obligatoire")
     * @Assert\Length(
     *      min = 10,
     *      minMessage = "Le champ Téléphone doit contenir {{ limit }} caractères")
     * @Assert\Regex(
     *     pattern="/^[0]{1}[1-79]{1}[0-9]{8}$/",
     *     htmlPattern="/^[0]{1}[1-79]{1}[0-9]{8}$/",
     *     message="Votre numéro de téléphone doit être au format 0xxxxxxxxx")
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Email est obligatoire")
     * @Assert\Email(message = "L'email '{{ value }}' est invalide.")
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Le champ Adresse est obligatoire")
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=5)
     * @Assert\NotBlank(message="Le champ Code postal est obligatoire")
     * @Assert\Length(
     *      min = 5,
     *      max = 5,
     *      minMessage = "Le champ Code postal doit contenir {{ limit }} chiffres",
     *      maxMessage = "Le champ Code postal doit contenir {{ limit }} chiffres")
     * @Assert\Regex(
     *     pattern="/^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))*([0-9]{3})?$/",
     *     htmlPattern = "/^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))*([0-9]{3})?$/",
     *     message="Le champ Code postal ne peut contenir que des chiffres")
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Ville est obligatoire")
     * @Assert\Regex(
     *     pattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     htmlPattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     message="Le champ Ville ne peut contenir que des lettres")
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nouveauPret;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $situationProjet;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $banque;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $empruntSeul;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $adresseIP;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sourceClient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeCampagne;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $urlReferrer;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $tracking;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $produit;

    /**
     * @return mixed
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param mixed $produit
     */
    public function setProduit($produit): void
    {
        $this->produit = $produit;
    }

    /**
     * @return mixed
     */
    public function getTracking()
    {
        return $this->tracking;
    }

    /**
     * @param mixed $tracking
     */
    public function setTracking($tracking): void
    {
        $this->tracking = $tracking;
    }

    /**
     * @return mixed
     */
    public function getCodeCampagne()
    {
        return $this->codeCampagne;
    }

    /**
     * @param mixed $codeCampagne
     */
    public function setCodeCampagne($codeCampagne): void
    {
        $this->codeCampagne = $codeCampagne;
    }

    /**
     * @return mixed
     */
    public function getUrlReferrer()
    {
        return $this->urlReferrer;
    }

    /**
     * @param mixed $urlReferrer
     */
    public function setUrlReferrer($urlReferrer): void
    {
        $this->urlReferrer = $urlReferrer;
    }

    /**
     * @return mixed
     */
    public function getSourceClient()
    {
        return $this->sourceClient;
    }

    /**
     * @param mixed $sourceClient
     */
    public function setSourceClient($sourceClient): void
    {
        $this->sourceClient = $sourceClient;
    }

    /**
     * @return mixed
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @param mixed $civilite
     */
    public function setCivilite($civilite): void
    {
        $this->civilite = $civilite;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone): void
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * @param mixed $cp
     */
    public function setCp($cp): void
    {
        $this->cp = $cp;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville): void
    {
        $this->ville = $ville;
    }

    /**
     * @return mixed
     */
    public function getNouveauPret()
    {
        return $this->nouveauPret;
    }

    /**
     * @param mixed $nouveauPret
     */
    public function setNouveauPret($nouveauPret): void
    {
        $this->nouveauPret = $nouveauPret;
    }

    /**
     * @return mixed
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param mixed $montant
     */
    public function setMontant($montant): void
    {
        $this->montant = $montant;
    }

    /**
     * @return mixed
     */
    public function getSituationProjet()
    {
        return $this->situationProjet;
    }

    /**
     * @param mixed $situationProjet
     */
    public function setSituationProjet($situationProjet): void
    {
        $this->situationProjet = $situationProjet;
    }

    /**
     * @return mixed
     */
    public function getBanque()
    {
        return $this->banque;
    }

    /**
     * @param mixed $banque
     */
    public function setBanque($banque): void
    {
        $this->banque = $banque;
    }

    /**
     * @return mixed
     */
    public function getEmpruntSeul()
    {
        return $this->empruntSeul;
    }

    /**
     * @param mixed $empruntSeul
     */
    public function setEmpruntSeul($empruntSeul): void
    {
        $this->empruntSeul = $empruntSeul;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getAdresseIP()
    {
        return $this->adresseIP;
    }

    /**
     * @param mixed $adresseIP
     */
    public function setAdresseIP($adresseIP): void
    {
        $this->adresseIP = $adresseIP;
    }

    /**
     * @return mixed
     */
    public function getOptinGenerali()
    {
        return $this->optinGenerali;
    }

    /**
     * @param mixed $optinGenerali
     */
    public function setOptinGenerali($optinGenerali): void
    {
        $this->optinGenerali = $optinGenerali;
    }

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $optinGenerali;

    public function getId(): ?int
    {
        return $this->id;
    }
}
