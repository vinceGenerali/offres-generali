<?php

namespace App\Entity;

use App\Repository\IdeoSanteRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=IdeoSanteRepository::class)
 */
class IdeoSante
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $civilite;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Nom est obligatoire")
     * @Assert\Regex(
     *     pattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     htmlPattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     message="Le champ Nom ne peut contenir que des lettres")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Prénom est obligatoire")
     * @Assert\Regex(
     *     pattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     htmlPattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     message="Le champ Prénom ne peut contenir que des lettres")
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank(message="Le champ Téléphone est obligatoire")
     * @Assert\Length(
     *      min = 10,
     *      minMessage = "Le champ Téléphone doit contenir {{ limit }} caractères")
     * @Assert\Regex(
     *     pattern="/^[0]{1}[1-79]{1}[0-9]{8}$/",
     *     htmlPattern="/^[0]{1}[1-79]{1}[0-9]{8}$/",
     *     message="Votre numéro de téléphone doit être au format 0xxxxxxxxx")
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Email est obligatoire")
     * @Assert\Email(message = "L'email '{{ value }}' est invalide.")
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Le champ Adresse est obligatoire")
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=5)
     * @Assert\NotBlank(message="Le champ Code postal est obligatoire")
     * @Assert\Length(
     *      min = 5,
     *      max = 5,
     *      minMessage = "Le champ Code postal doit contenir {{ limit }} chiffres",
     *      maxMessage = "Le champ Code postal doit contenir {{ limit }} chiffres")
     * @Assert\Regex(
     *     pattern="/^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))*([0-9]{3})?$/",
     *     htmlPattern = "/^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))*([0-9]{3})?$/",
     *     message="Le champ Code postal ne peut contenir que des chiffres")
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Ville est obligatoire")
     * @Assert\Regex(
     *     pattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     htmlPattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     message="Le champ Ville ne peut contenir que des lettres")
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $raisonSociale;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $statut;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $regime;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activite;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $adresseIP;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $optinGenerali;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sourceClient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeCampagne;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $urlReferrer;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $tracking;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $canal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $familleProduit;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $identifiantPartenaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $produit;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $reseau;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $conqueteLeadTechnicalId;

    /**
     * @return mixed
     */
    public function getCodeCampagne()
    {
        return $this->codeCampagne;
    }

    /**
     * @param mixed $codeCampagne
     */
    public function setCodeCampagne($codeCampagne): void
    {
        $this->codeCampagne = $codeCampagne;
    }

    /**
     * @return mixed
     */
    public function getUrlReferrer()
    {
        return $this->urlReferrer;
    }

    /**
     * @param mixed $urlReferrer
     */
    public function setUrlReferrer($urlReferrer): void
    {
        $this->urlReferrer = $urlReferrer;
    }

    /**
     * @return mixed
     */
    public function getTracking()
    {
        return $this->tracking;
    }

    /**
     * @param mixed $tracking
     */
    public function setTracking($tracking): void
    {
        $this->tracking = $tracking;
    }

    /**
     * @return mixed
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param mixed $produit
     */
    public function setProduit($produit): void
    {
        $this->produit = $produit;
    }

    /**
     * @return mixed
     */
    public function getSourceClient()
    {
        return $this->sourceClient;
    }

    /**
     * @param mixed $sourceClient
     */
    public function setSourceClient($sourceClient): void
    {
        $this->sourceClient = $sourceClient;
    }

    /**
     * @return mixed
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @param mixed $civilite
     */
    public function setCivilite($civilite): void
    {
        $this->civilite = $civilite;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone): void
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getRaisonSociale()
    {
        return $this->raisonSociale;
    }

    /**
     * @param mixed $raisonSociale
     */
    public function setRaisonSociale($raisonSociale): void
    {
        $this->raisonSociale = $raisonSociale;
    }

    /**
     * @return mixed
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * @param mixed $dateNaissance
     */
    public function setDateNaissance($dateNaissance): void
    {
        $this->dateNaissance = $dateNaissance;
    }

    /**
     * @return mixed
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param mixed $statut
     */
    public function setStatut($statut): void
    {
        $this->statut = $statut;
    }

    /**
     * @return mixed
     */
    public function getRegime()
    {
        return $this->regime;
    }

    /**
     * @param mixed $regime
     */
    public function setRegime($regime): void
    {
        $this->regime = $regime;
    }

    /**
     * @return mixed
     */
    public function getActivite()
    {
        return $this->activite;
    }

    /**
     * @param mixed $activite
     */
    public function setActivite($activite): void
    {
        $this->activite = $activite;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getAdresseIP()
    {
        return $this->adresseIP;
    }

    /**
     * @param mixed $adresseIP
     */
    public function setAdresseIP($adresseIP): void
    {
        $this->adresseIP = $adresseIP;
    }

    /**
     * @return mixed
     */
    public function getOptinGenerali()
    {
        return $this->optinGenerali;
    }

    /**
     * @param mixed $optinGenerali
     */
    public function setOptinGenerali($optinGenerali): void
    {
        $this->optinGenerali = $optinGenerali;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * @param mixed $cp
     */
    public function setCp($cp): void
    {
        $this->cp = $cp;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville): void
    {
        $this->ville = $ville;
    }

    public function getCanal(): ?string
    {
        return $this->canal;
    }

    public function setCanal(?string $canal): self
    {
        $this->canal = $canal;

        return $this;
    }

    public function getFamilleProduit(): ?string
    {
        return $this->familleProduit;
    }

    public function setFamilleProduit(?string $familleProduit): self
    {
        $this->familleProduit = $familleProduit;

        return $this;
    }

    public function getIdentifiantPartenaire(): ?string
    {
        return $this->identifiantPartenaire;
    }

    public function setIdentifiantPartenaire(?string $identifiantPartenaire): self
    {
        $this->identifiantPartenaire = $identifiantPartenaire;

        return $this;
    }

    public function getReseau(): ?string
    {
        return $this->reseau;
    }

    public function setReseau(?string $reseau): self
    {
        $this->reseau = $reseau;

        return $this;
    }

    public function getConqueteLeadTechnicalId(): ?string
    {
        return $this->conqueteLeadTechnicalId;
    }

    public function setConqueteLeadTechnicalId(?string $conqueteLeadTechnicalId): self
    {
        $this->conqueteLeadTechnicalId = $conqueteLeadTechnicalId;

        return $this;
    }
}
