<?php

namespace App\Entity;

use App\Repository\ListeFormsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ListeFormsRepository::class)
 */
class ListeForms
{
    public function __toString()
    {
        return (string)$this->getNomFormulaire();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NomFormulaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NomProduit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $UniversProduit;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DateCreation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $urlFormulaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $urlAdminFormulaire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $visuelEntete;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logoProduit;

    /**
     * @return mixed
     */
    public function getLogoProduit()
    {
        return $this->logoProduit;
    }

    /**
     * @ORM\OneToMany(targetEntity=ArgumentProduit::class, mappedBy="idFormulaire")
     */
    private $argumentProduits;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pictoMenu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $campagne;

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @param mixed $logoProduit
     */
    public function setLogoProduit($logoProduit): void
    {
        $this->logoProduit = $logoProduit;
    }

    /**
     * @param ArrayCollection $argumentProduits
     */
    public function setArgumentProduits(ArrayCollection $argumentProduits): void
    {
        $this->argumentProduits = $argumentProduits;
    }

    /**
     * @param mixed $campagne
     */
    public function setCampagne($campagne): void
    {
        $this->campagne = $campagne;
    }

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tcFormName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tcFormNameMerci;

    /**
     * @return mixed
     */
    public function getTcFormNameMerci()
    {
        return $this->tcFormNameMerci;
    }

    /**
     * @param mixed $tcFormNameMerci
     */
    public function setTcFormNameMerci($tcFormNameMerci): void
    {
        $this->tcFormNameMerci = $tcFormNameMerci;
    }

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tcProductCategory;

    /**
     * @return mixed
     */
    public function getCampagne()
    {
        return $this->campagne;
    }

    public function __construct()
    {
        $this->argumentProduits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomFormulaire(): ?string
    {
        return $this->NomFormulaire;
    }

    public function setNomFormulaire(string $NomFormulaire): self
    {
        $this->NomFormulaire = $NomFormulaire;

        return $this;
    }

    public function getNomProduit(): ?string
    {
        return $this->NomProduit;
    }

    public function setNomProduit(string $NomProduit): self
    {
        $this->NomProduit = $NomProduit;

        return $this;
    }

    public function getUniversProduit(): ?string
    {
        return $this->UniversProduit;
    }

    public function setUniversProduit(string $UniversProduit): self
    {
        $this->UniversProduit = $UniversProduit;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->DateCreation;
    }

    public function setDateCreation(\DateTimeInterface $DateCreation): self
    {
        $this->DateCreation = $DateCreation;

        return $this;
    }

    public function getUrlFormulaire(): ?string
    {
        return $this->urlFormulaire;
    }

    public function setUrlFormulaire(string $urlFormulaire): self
    {
        $this->urlFormulaire = $urlFormulaire;

        return $this;
    }

    public function getUrlAdminFormulaire(): ?string
    {
        return $this->urlAdminFormulaire;
    }

    public function setUrlAdminFormulaire(string $urlAdminFormulaire): self
    {
        $this->urlAdminFormulaire = $urlAdminFormulaire;

        return $this;
    }

    public function getVisuelEntete(): ?string
    {
        return $this->visuelEntete;
    }

    public function setVisuelEntete(?string $visuelEntete): self
    {
        $this->visuelEntete = $visuelEntete;

        return $this;
    }

    /**
     * @return Collection|ArgumentProduit[]
     */
    public function getArgumentProduits(): Collection
    {
        return $this->argumentProduits;
    }

    public function addArgumentProduit(ArgumentProduit $argumentProduit): self
    {
        if (!$this->argumentProduits->contains($argumentProduit)) {
            $this->argumentProduits[] = $argumentProduit;
            $argumentProduit->setIdFormulaire($this);
        }

        return $this;
    }

    public function removeArgumentProduit(ArgumentProduit $argumentProduit): self
    {
        if ($this->argumentProduits->contains($argumentProduit)) {
            $this->argumentProduits->removeElement($argumentProduit);
            // set the owning side to null (unless already changed)
            if ($argumentProduit->getIdFormulaire() === $this) {
                $argumentProduit->setIdFormulaire(null);
            }
        }

        return $this;
    }

    public function getPictoMenu(): ?string
    {
        return $this->pictoMenu;
    }

    public function setPictoMenu(string $pictoMenu): self
    {
        $this->pictoMenu = $pictoMenu;

        return $this;
    }

    public function getTcFormName(): ?string
    {
        return $this->tcFormName;
    }

    public function setTcFormName(?string $tcFormName): self
    {
        $this->tcFormName = $tcFormName;

        return $this;
    }

    public function getTcProductCategory(): ?string
    {
        return $this->tcProductCategory;
    }

    public function setTcProductCategory(?string $tcProductCategory): self
    {
        $this->tcProductCategory = $tcProductCategory;

        return $this;
    }

}
