<?php

namespace App\Entity;

use App\Repository\HabitationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=HabitationRepository::class)
 */
class Habitation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $civilite;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Nom est obligatoire")
     * @Assert\Regex(
     *     pattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     htmlPattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     message="Le champ Nom ne peut contenir que des lettres")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Prénom est obligatoire")
     * @Assert\Regex(
     *     pattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     htmlPattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     message="Le champ Prénom ne peut contenir que des lettres")
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank(message="Le champ Téléphone est obligatoire")
     * @Assert\Length(
     *      min = 10,
     *      minMessage = "Le champ Téléphone doit contenir {{ limit }} caractères")
     * @Assert\Regex(
     *     pattern="/^[0]{1}[1-79]{1}[0-9]{8}$/",
     *     htmlPattern="/^[0]{1}[1-79]{1}[0-9]{8}$/",
     *     message="Votre numéro de téléphone doit être au format 0xxxxxxxxx")
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Email est obligatoire")
     * @Assert\Email(message = "L'email '{{ value }}' est invalide.")
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Le champ Adresse est obligatoire")
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=5)
     * @Assert\NotBlank(message="Le champ Code postal est obligatoire")
     * @Assert\Length(
     *      min = 5,
     *      max = 5,
     *      minMessage = "Le champ Code postal doit contenir {{ limit }} chiffres",
     *      maxMessage = "Le champ Code postal doit contenir {{ limit }} chiffres")
     * @Assert\Regex(
     *     pattern="/^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))*([0-9]{3})?$/",
     *     htmlPattern = "/^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))*([0-9]{3})?$/",
     *     message="Le champ Code postal ne peut contenir que des chiffres")
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ Ville est obligatoire")
     * @Assert\Regex(
     *     pattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     htmlPattern="/[A-Za-zéèêëàâîïôöûü-]+/",
     *     message="Le champ Ville ne peut contenir que des lettres")
     */
    private $ville;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $adresseIP;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $optinGenerali;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $statutOccupant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $typeHabitation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sourceClient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeCampagne;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $urlReferrer;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $tracking;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $produit;

    /**
     * @return mixed
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param mixed $produit
     */
    public function setProduit($produit): void
    {
        $this->produit = $produit;
    }

    /**
     * @return mixed
     */
    public function getTracking()
    {
        return $this->tracking;
    }

    /**
     * @param mixed $tracking
     */
    public function setTracking($tracking): void
    {
        $this->tracking = $tracking;
    }

    /**
     * @return mixed
     */
    public function getCodeCampagne()
    {
        return $this->codeCampagne;
    }

    /**
     * @param mixed $codeCampagne
     */
    public function setCodeCampagne($codeCampagne): void
    {
        $this->codeCampagne = $codeCampagne;
    }

    /**
     * @return mixed
     */
    public function getUrlReferrer()
    {
        return $this->urlReferrer;
    }

    /**
     * @param mixed $urlReferrer
     */
    public function setUrlReferrer($urlReferrer): void
    {
        $this->urlReferrer = $urlReferrer;
    }

    /**
     * @return mixed
     */
    public function getSourceClient()
    {
        return $this->sourceClient;
    }

    /**
     * @param mixed $sourceClient
     */
    public function setSourceClient($sourceClient): void
    {
        $this->sourceClient = $sourceClient;
    }

    /**
     * @return mixed
     */
    public function getOptinGenerali()
    {
        return $this->optinGenerali;
    }

    /**
     * @param mixed $optinGenerali
     */
    public function setOptinGenerali($optinGenerali): void
    {
        $this->optinGenerali = $optinGenerali;
    }

    /**
     * @return mixed
     */
    public function getdate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setdate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getAdresseIP()
    {
        return $this->adresseIP;
    }

    /**
     * @param mixed $adresseIP
     */
    public function setAdresseIP($adresseIP): void
    {
        $this->adresseIP = $adresseIP;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getStatutOccupant(): ?string
    {
        return $this->statutOccupant;
    }

    public function setStatutOccupant(string $statutOccupant): self
    {
        $this->statutOccupant = $statutOccupant;

        return $this;
    }

    public function getTypeHabitation(): ?string
    {
        return $this->typeHabitation;
    }

    public function setTypeHabitation(string $typeHabitation): self
    {
        $this->typeHabitation = $typeHabitation;

        return $this;
    }
}
