<?php


namespace App\Utils;

/**
 * Classe utilisée pour la récupération et la concaténation des éléments nécessaires .
 *
 * @author Vincent PETIT <vincent.petit@generali.com>
 */
class ConstructionParametres
{
    public function constructionTracking($request): string
    {
        $zanpid = (!empty($request->query->get('zanpid')) ? $request->query->get('zanpid') : '');

        if (!empty($request->query->get('utm_source') && $request->query->get('utm_source') == 'kwanko')) {
            $tcId = (!empty($request->cookies->get('tc_id_unique')) ? $request->cookies->get('tc_id_unique') : '');
            return $tcId . '-' . $zanpid;
        } else {
            return $zanpid;
        }
    }

    public function getReferer($request): string
    {
        $referer = $request->headers->get('referer');
        if (!is_string($referer) || !$referer) {
            $referer = '';
        }
        return substr($referer, 0, 20);
    }
}