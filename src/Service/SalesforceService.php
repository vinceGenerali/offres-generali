<?php

namespace App\Service;

use App\Entity\Auto;
use App\Entity\ChienChat;
use App\Entity\Emprunteur;
use App\Entity\Habitation;
use App\Entity\SanteSenior;
use Exception;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SalesforceService
{
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;
    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $httpClient;

    /**
     * @param HttpClientInterface $httpClient
     * @param SerializerInterface $serializer
     */
    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serializer)
    {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    /**
     * @return array
     * @throws DecodingExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function connectAction(): array
    {
        $url_token = $_ENV['OAUTH_SALESFORCE_TOKEN_ENDPOINT'];
        $token_data = [
            "grant_type" => 'password',
            "client_id" => $_ENV['OAUTH_SALESFORCE_CLIENT_ID'],
            "client_secret" => $_ENV['OAUTH_SALESFORCE_CLIENT_SECRET'],
            "username" => $_ENV['OAUTH_SALESFORCE_CLIENT_USERNAME'],
            "password" => $_ENV['OAUTH_SALESFORCE_CLIENT_PASSWORD']
        ];
        try {
            $response = $this->httpClient->request('POST', $url_token, [
                'timeout' => 5,
                'body' => $token_data,
                'verify_peer' => false,
                'headers' => [
                    'Content-type:application/x-www-form-urlencoded'
                ],
            ]);
            $content = $response->toArray();
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
        }
        if (200 !== $response->getStatusCode()) {
            throw new Exception('Impossible de réaliser la requête de connexion');
        }
        return $content;
    }

    /**
     * @param $formData
     * @return string|void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function demandeActionApex($formData)
    {
        $result_token = $this->connectAction();
        if (isset($result_token['instance_url']) && !empty($result_token['instance_url'])) {
            $endpoint_url = $result_token['instance_url'] . '/services/apexrest/Demande';
            $access_token = $result_token['access_token'];
        }
        $optin = (false === $formData->getOptinGenerali()) ? 0 : 1;
        $demandeGeneric = [
            'demande' => [
                'callback_date' => date(DATE_ATOM),
                'SourceClient' => $formData->getSourceClient(),
                // Valeur a renseigner selon projet
                'Source' => 'Autre',
                // Valeur a renseigner selon projet
                'DateLead' => date('d/m/Y-H:i'),
                'Produit' => $formData->getProduit(),
                // Valeur a renseigner selon projet
                'FamilleProduit' => 'IARD',
                // Valeur a renseigner selon projet, mettre NON_RENSEIGNE si pas de valeur
                'Formulaire' => 'LP',
                // Valeur a modififer
                'libelleFormulaire' => 'Demande de rappel PMC IARD',
                'prioritaire' => 'non',
                'IndicateurAutorisationInformation' => $optin,
                // Valeur a renseigner selon projet
                'Campagne' => $formData->getCodeCampagne(),
                'Tracking' => $formData->getTracking(),
                'urlReferreur' => $formData->getUrlReferrer(),
                // Valeur a renseigner selon projet, en fonction de l env et du reseau
                'Civilite' => $formData->getCivilite(),
                'nom' => $formData->getNom(),
                'prenom' => $formData->getPrenom(),
                'email' => $formData->getEmail(),
                'destination' => $formData->getTelephone(),
                'nomVoie' => $formData->getAdresse(),
                'codePostal' => $formData->getCp(),
                'ville' => $formData->getVille(),
            ]
        ];
        if ($formData instanceof Habitation) {
            $demande_data = [
                'demande' => [
                    'callback_date' => date(DATE_ATOM),
                    'SourceClient' => $formData->getSourceClient(),
                    // Valeur a renseigner selon projet
                    'Source' => 'Autre',
                    // Valeur a renseigner selon projet
                    'DateLead' => date('d/m/Y-H:i'),
                    'Produit' => $formData->getProduit(),
                    // Valeur a renseigner selon projet
                    'FamilleProduit' => 'IARD',
                    // Valeur a renseigner selon projet, mettre NON_RENSEIGNE si pas de valeur
                    'Formulaire' => 'LP',
                    // Valeur a modififer
                    'libelleFormulaire' => 'Demande de rappel PMC IARD',
                    'prioritaire' => 'non',
                    'IndicateurAutorisationInformation' => false,
                    // Valeur a renseigner selon projet
                    'Campagne' => $formData->getCodeCampagne(),
                    'Tracking' => $formData->getTracking(),
                    'urlReferreur' => $formData->getUrlReferrer(),
                    // Valeur a renseigner selon projet, en fonction de l env et du reseau
                    'Civilite' => $formData->getCivilite(),
                    'nom' => $formData->getNom(),
                    'prenom' => $formData->getPrenom(),
                    'email' => $formData->getEmail(),
                    'destination' => $formData->getTelephone(),
                    'nomVoie' => $formData->getAdresse(),
                    'codePostal' => $formData->getCp(),
                    'ville' => $formData->getVille(),
                    'natureOccupation' => $formData->getStatutOccupant(),
                    'typeHabitation' => $formData->getTypeHabitation(),
                ]
            ];
        }
        if ($formData instanceof Auto) {
            $demande_data = [
                'demande' => [
                    'callback_date' => date(DATE_ATOM),
                    'SourceClient' => $formData->getSourceClient(),
                    // Valeur a renseigner selon projet
                    'Source' => 'Autre',
                    // Valeur a renseigner selon projet
                    'DateLead' => date('d/m/Y-H:i'),
                    'Produit' => $formData->getProduit(),
                    // Valeur a renseigner selon projet
                    'FamilleProduit' => 'IARD',
                    // Valeur a renseigner selon projet, mettre NON_RENSEIGNE si pas de valeur
                    'Formulaire' => 'LP',
                    // Valeur a modififer
                    'libelleFormulaire' => 'Demande de rappel PMC IARD',
                    'prioritaire' => 'non',
                    'IndicateurAutorisationInformation' => $formData->getoptinGenerali(),
                    // Valeur a renseigner selon projet
                    'Campagne' => $formData->getCodeCampagne(),
                    'Tracking' => $formData->getTracking(),
                    'urlReferreur' => $formData->getUrlReferrer(),
                    // Valeur a renseigner selon projet, en fonction de l env et du reseau
                    'Civilite' => $formData->getCivilite(),
                    'nom' => $formData->getNom(),
                    'prenom' => $formData->getPrenom(),
                    'email' => $formData->getEmail(),
                    'destination' => $formData->getTelephone(),
                    'nomVoie' => $formData->getAdresse(),
                    'codePostal' => $formData->getCp(),
                    'ville' => $formData->getVille(),
                    'date_mise_circulation' => $formData->getDateMiseEnCirculation(),
                    'date_permis' => $formData->getDateobtentionPermis(),
                    'dateEffet' => $formData->getDateEffetContrat(),
                    'code_postal_garage' => $formData->getLieuGarage(),
                    'usageVehicule' => $formData->getCasUtilisationVehicule()
                ]
            ];
        }
        if ($formData instanceof SanteSenior) {
            $demande_data = [
                'demande' => [
                    'callback_date' => date(DATE_ATOM),
                    'SourceClient' => $formData->getSourceClient(),
                    // Valeur a renseigner selon projet
                    'Source' => 'Autre',
                    // Valeur a renseigner selon projet
                    'DateLead' => date('d/m/Y-H:i'),
                    'Produit' => $formData->getProduit(),
                    // Valeur a renseigner selon projet
                    'FamilleProduit' => 'IARD',
                    // Valeur a renseigner selon projet, mettre NON_RENSEIGNE si pas de valeur
                    'Formulaire' => 'LP',
                    // Valeur a modififer
                    'libelleFormulaire' => 'Demande de rappel PMC IARD',
                    'prioritaire' => 'non',
                    'IndicateurAutorisationInformation' => false,
                    // Valeur a renseigner selon projet
                    'Campagne' => $formData->getCodeCampagne(),
                    'Tracking' => $formData->getTracking(),
                    'urlReferreur' => $formData->getUrlReferrer(),
                    // Valeur a renseigner selon projet, en fonction de l env et du reseau
                    'Civilite' => $formData->getCivilite(),
                    'nom' => $formData->getNom(),
                    'prenom' => $formData->getPrenom(),
                    'email' => $formData->getEmail(),
                    'destination' => $formData->getTelephone(),
                    'nomVoie' => $formData->getAdresse(),
                    'codePostal' => $formData->getCp(),
                    'ville' => $formData->getVille(),
                    'dateDeNaissance' => $formData->getDateNaissance()
                ]
            ];
        }
        if ($formData instanceof ChienChat) {
            $demande_data = $demandeGeneric;
        }
        if ($formData instanceof Emprunteur) {
            $demande_data = $demandeGeneric;
        }
        $demande_json = $this->serializer->serialize($demande_data, 'json');
        try {
            $response = $this->httpClient->request('POST', $endpoint_url, [
                'timeout' => 5,
                'body' => $demande_json,
                'verify_peer' => false,
                'headers' => [
                    'Content-Type:application/json',
                    'Authorization:Bearer ' . $access_token,
                ],
            ]);
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
        }
        if (200 !== $response->getStatusCode()) {
            throw new Exception('Impossible de réaliser la requête pour insertion');
        } else {
            return ($response->getContent());
        }
    }
}
