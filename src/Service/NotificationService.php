<?php

namespace App\Service;

use App\Entity\JeuConcoursCollecte;
use Swift_Mailer;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class NotificationService
 */
class NotificationService
{
    /**
     * @var Swift_Mailer
     */
    private Swift_Mailer $mailer;

    /**
     * @var Environment
     */
    private Environment $renderer;

    /**
     * ContactNotification constructor.
     * @param Swift_Mailer $mailer
     * @param Environment $renderer
     */
    public function __construct(Swift_Mailer $mailer, Environment $renderer)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    /**
     * @param JeuConcoursCollecte $jeuConcoursCollecte
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function notifyGrattageGagnant(JeuConcoursCollecte $jeuConcoursCollecte, $libelleLot)
    {
        $message = (new \Swift_Message('Jeu concours Generali'))
            ->setFrom('no-reply@generali.fr')
            ->setTo($jeuConcoursCollecte->getEmail())
            ->setReplyTo('no-reply@generali.fr')
            ->setBody(
                $this->renderer->render(
                    'emails/jeuconcours2021-grattage-gagnant.html.twig',[
                        'lot'=>$libelleLot
                ]),
                'text/html'
            );
        $this->mailer->send($message);
    }

    /**
     * @param JeuConcoursCollecte $jeuConcoursCollecte
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function notifyTirage(JeuConcoursCollecte $jeuConcoursCollecte)
    {
        $message = (new \Swift_Message('Jeu concours Generali'))
            ->setFrom('no-reply@generali.fr')
            ->setTo($jeuConcoursCollecte->getEmail())
            ->setReplyTo('no-reply@generali.fr')
            ->setBody(
                $this->renderer->render(
                    'emails/jeuconcours2021-tirage.html.twig',[
                ]),
                'text/html'
            );
        $this->mailer->send($message);
    }
}
