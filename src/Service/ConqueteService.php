<?php

namespace App\Service;

use App\Entity\Gpn;
use App\Entity\IdeoSante;
use App\Entity\MRC;
use App\Entity\Prevoyance;
use App\Entity\Retraite;
use App\Entity\Spa;
use Exception;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ConqueteService
{
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;
    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $httpClient;

    /**
     * ConqueteController constructor.
     * @param HttpClientInterface $httpClient
     * @param SerializerInterface $serializer
     * @throws Exception|Exception
     */
    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serializer)
    {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    /**
     * @return array
     * @throws DecodingExceptionInterface
     * @throws TransportExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function connectAction()
    {
        $url_token = $_ENV['OAUTH_CONQUETE_TOKEN_ENDPOINT'];
        $token_data = [
            "client_id" => $_ENV['OAUTH_CONQUETE_CLIENT_ID'],
            "client_secret" => $_ENV['OAUTH_CONQUETE_CLIENT_SECRET']
        ];
        try {
            $response = $this->httpClient->request('POST', $url_token, [
                'timeout' => 5,
                'body' => $token_data,
                'verify_peer' => false,
                'headers' => [
                    'Content-type:application/x-www-form-urlencoded'
                ],
            ]);
            $content = $response->toArray();
        } catch (ClientExceptionInterface $e) {
        }
        if (200 !== $response->getStatusCode()) {
            throw new Exception('Impossible de réaliser la requête de connexion');
        }
        return $content;
    }

    /**
     * @param $formData
     * @return string
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function postDataAction($formData)
    {
        $result_token = $this->connectAction();
        $access_token = $result_token['access_token'];
        $endpoint_url = $_ENV['OAUTH_CONQUETE_IMPORT_ENDPOINT'];
        $optinCourrier = (false === $formData->getOptinGenerali()) ? 0 : 1;
        $optinEmail = (false === $formData->getOptinGenerali()) ? 0 : 1;
        $optinTel = (false === $formData->getOptinGenerali()) ? 0 : 1;

        if ($formData instanceof Spa) {
            $dtoQuestionReponse = [
                [
                    'question' => "Type Animal",
                    'reponse' => $formData->getTypeAnimal()
                ],
                [
                    'question' => "Date de naissance animal",
                    'reponse' => $formData->getDateNaissanceAnimal()
                ]
            ];
        }
        if ($formData instanceof Gpn) {
            $dtoQuestionReponse = [
                [
                    'question' => "Secteur d'activite",
                    'reponse' => $formData->getActivite()
                ],
                [
                    'question' => "CA",
                    'reponse' => $formData->getCA()
                ],
                [
                    'question' => "Effectif",
                    'reponse' => $formData->getNombreSalarie()
                ],
                [
                    'question' => "Commercialisez-vous des produits et/ou des services sur internet",
                    'reponse' => $formData->getECommercant()
                ]
            ];
        }
        if ($formData instanceof IdeoSante) {
            // Formulaire Idéo Santé
            $dtoQuestionReponse = [
                [
                    'question' => "Date de naissance",
                    'reponse' => $formData->getDateNaissance()
                ],
                [
                    'question' => "Statut",
                    'reponse' => $formData->getStatut()
                ],
                [
                    'question' => "Activite",
                    'reponse' => $formData->getActivite()
                ],
                [
                    'question' => "Regime social",
                    'reponse' => $formData->getRegime()
                ],
            ];
        }
        if ($formData instanceof Retraite) {
            // Formulaire Retraite
            $dtoQuestionReponse = [
                [
                    'question' => "Date de naissance",
                    'reponse' => $formData->getDateNaissance()
                ],
                [
                    'question' => "Statut",
                    'reponse' => $formData->getStatut()
                ],
                [
                    'question' => "Activite",
                    'reponse' => $formData->getActivite()
                ],
                [
                    'question' => "Age depart à la retraite",
                    'reponse' => $formData->getSouhaitAgeRetraite()
                ],
                [
                    'question' => "Capacite Epargne",
                    'reponse' => $formData->getCapaciteEpargne()
                ],
            ];
        }
        if ($formData instanceof MRC) {
            // Formulaire MRC
            $dtoQuestionReponse = [
                [
                    'question' => "Activité",
                    'reponse' => $formData->getActivite()
                ],
                [
                    'question' => "Statut occupation",
                    'reponse' => $formData->getActivite()
                ],
                [
                    'question' => "Superficie des locaux",
                    'reponse' => $formData->getSuperficie()
                ],
                [
                    'question' => "Valeur du mobilier, matériels, machines et stock à assurer",
                    'reponse' => $formData->getValeurMobilier()
                ],
                [
                    'question' => "Effectif",
                    'reponse' => $formData->getNombreSalarie()
                ],
                [
                    'question' => "CA",
                    'reponse' => $formData->getCA()
                ]
            ];
        }
        if ($formData instanceof Prevoyance) {
            // Formulaire Prévoyance
            $renteConjoint = (false === $formData->getRenteConjoint()) ? 0 : 1;
            $renteEtudiant = (false === $formData->getRenteEtudiant()) ? 0 : 1;
            $dtoQuestionReponse = [
                [
                    'question' => "Statut",
                    'reponse' => $formData->getStatut()
                ],
                [
                    'question' => "Activite",
                    'reponse' => $formData->getActivite()
                ],
                [
                    'question' => "Regime social",
                    'reponse' => $formData->getRegime()
                ],
                [
                    'question' => "Assurance Prévoyance recherchée",
                    'reponse' => $formData->getTypeAssuranceRecherche()
                ],
                [
                    'question' => "Rente Conjoint",
                    'reponse' => $renteConjoint
                ],
                [
                    'question' => "Rente Etudiant",
                    'reponse' => $renteEtudiant
                ]
            ];
        }
        $formatDemande = [
            'demande' => [
                'dtoLead' => [
                    'adresse3' => $formData->getAdresse(),
                    'campagne' => $formData->getCodeCampagne(),
                    'canal' => 'Generali.fr',
                    'civilite' => $formData->getCivilite(),
                    'codePostal' => $formData->getCp(),
                    'dateLead' => date('Y-m-d'),
                    'familleProduit' => '01',
                    'identifiantPartenaire' => $formData->getIdentifiantPartenaire(),
                    'mail' => $formData->getEmail(),
                    'mobile' => $formData->getTelephone(),
                    'nom' => $formData->getNom(),
                    'optInCourrier' => $optinCourrier,
                    'optInEmail' => $optinEmail,
                    'optInTel' => $optinTel,
                    'preNom' => $formData->getPrenom(),
                    'produit' =>  $formData->getProduit(),
                    'reseau' => 'agent',
                    'source' => $formData->getSourceClient(),
                    'telephone' => $formData->getTelephone(),
                    'tracking' => $formData->getTracking(),
                    'urlReferreur' => '',
                    'ville' => $formData->getVille(),
                    'dtoQuestionReponseListe' => [
                        'dtoQuestionReponse' => $dtoQuestionReponse,
                    ]
                ]
            ]
        ];
        $demande_json = $this->serializer->serialize($formatDemande, 'json');
        //dd($demande_json);
        try {
            $response = $this->httpClient->request('POST', $endpoint_url, [
                'timeout' => 5,
                'body' => $demande_json,
                'verify_peer' => true,
                'headers' => [
                    'Content-Type:application/json',
                    'Authorization:Bearer '.$access_token,
                ],
            ]);
        } catch (
        ClientExceptionInterface |
        RedirectionExceptionInterface |
        ServerExceptionInterface |
        TransportExceptionInterface $e
        ) {
        }
        if (200 !== $response->getStatusCode()) {
            throw new Exception('Impossible d\'insérer les données.');
        } else {
            $array_reponse = $response->toArray();
            return ($array_reponse['importerLeadResponse']['return']['leadTechnicalId']);
        }
    }
}
