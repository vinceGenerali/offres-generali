<?php

namespace App\Controller;

use App\Controller\salesforce\SalesforceController;
use App\Entity\ChienChat;
use App\Form\ChienChatType;
use App\Repository\ChienChatRepository;
use App\Repository\ListeFormsRepository;
use App\Service\SalesforceService;
use App\Utils\ConstructionParametres;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;

class ChienChatController extends AbstractController
{
    /**
     * @Route("/assurance-chien-chat", name="app_chien_chat")
     * @param Request $request
     * @param ListeFormsRepository $repository
     * @param SalesforceController $salesforce
     * @param ConstructionParametres $constructionParametres
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function index(
        Request $request,
        ListeFormsRepository $repository,
        SalesforceService $salesforce,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    )
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(8);
        $animal = new ChienChat();
        $form = $this->createForm(ChienChatType::class, $animal);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $animal->setdate(new DateTime());
            $animal->setAdresseIP($request->getClientIp());
            $animal->setSourceClient('Action Payante Chien / Chat');
            $animal->setCodeCampagne($optionForm->getCampagne());
            $animal->setProduit($optionForm->getNomFormulaire());
            $animal->setUrlReferrer($referer);
            $animal->setTracking($tracking);
            $salesforce->demandeActionApex($animal);
            $entityManager->persist($animal);
            $entityManager->flush();

            //$notification->notifyInscription($animal->getEmail(), $animal->getCivilite(), $animal->getNom(), $animal->getPrenom());

            return $this->redirectToRoute("app_chien_chat_confirmation");
        }
        return $this->render('chien_chat/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-assurance-chien-chat", name="app_chien_chat_confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(8);
        return $this->render('chien_chat/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("/admin/formulaire/assurance-chien-chat", name="admin_form_chien_chat")
     * @param ListeFormsRepository $listeFormsRepository
     * @param ChienChatRepository $repository
     * @return RedirectResponse|Response
     */
    public function indexAdmin(ListeFormsRepository $listeFormsRepository, ChienChatRepository $repository)
    {
        $leads = $repository->findAll();
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/formulaire/chien_chat/index.html.twig', [
            'Titre' => 'Formulaire Chien Chat',
            'leads' => $leads,
            'dataForms' => $dataForms
        ]);
    }
}
