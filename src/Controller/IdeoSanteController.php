<?php

namespace App\Controller;

use App\Controller\conquete\ConqueteController;
use App\Entity\IdeoSante;
use App\Form\IdeoSanteType;
use App\Repository\ListeFormsRepository;
use App\Repository\IdeoSanteRepository;
use App\Service\ConqueteService;
use App\Utils\ConstructionParametres;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class IdeoSanteController extends AbstractController
{
    /**
     * @Route("/pro-complementaire-sante", name="app_ideo_sante")
     * @param Request $request
     * @param ListeFormsRepository $repository
     * @param ConqueteService $conqueteService
     * @param EntityManagerInterface $entityManager
     * @param ConstructionParametres $constructionParametres
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(
        Request $request,
        ListeFormsRepository $repository,
        ConqueteService $conqueteService,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    )
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(4);
        $sante = new IdeoSante();
        $form = $this->createForm(IdeoSanteType::class, $sante);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $sante->setDate(new DateTime());
            $sante->setAdresseIP($request->getClientIp());
            $sante->setSourceClient('Demande de rappel PRO');
            $sante->setCodeCampagne($optionForm->getCampagne());
            $sante->setProduit($optionForm->getNomProduit());
            $sante->setCanal('');
            $sante->setIdentifiantPartenaire(substr(uniqid('SAN'),0, 10 ));
            $sante->setUrlReferrer($referer);
            $sante->setTracking($tracking);
            $conqueteLeadTechnicalId = $conqueteService->postDataAction($sante);
            $sante->setConqueteLeadTechnicalId($conqueteLeadTechnicalId);
            $entityManager->persist($sante);
            $entityManager->flush();

            //$notification->notifyInscription($sante->getEmail(), $sante->getCivilite(), $sante->getNom(), $sante->getPrenom());

            return $this->redirectToRoute("app_ideo_sante_confirmation");
        }

        return $this->render('ideo_sante/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-pro-complementaire-sante", name="app_ideo_sante_confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(4);
        return $this->render('ideo_sante/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("/admin/formulaire/complementaire-sante-pros", name="admin_ideo_sante")
     * @param ListeFormsRepository $listeFormsRepository
     * @param IdeoSanteRepository $repository
     * @return RedirectResponse|Response
     */
    public function indexAdmin(ListeFormsRepository $listeFormsRepository, IdeoSanteRepository $repository)
    {
        $leads = $repository->findAll();
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/formulaire/ideo_sante/index.html.twig', [
            'Titre' => 'Formulaire Ideo Santé',
            'leads' => $leads,
            'dataForms' => $dataForms
        ]);
    }
}
