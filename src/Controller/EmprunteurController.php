<?php

namespace App\Controller;

use App\Controller\salesforce\SalesforceController;
use App\Entity\Emprunteur;
use App\Form\EmprunteurType;
use App\Repository\EmprunteurRepository;
use App\Repository\ListeFormsRepository;
use App\Service\SalesforceService;
use App\Utils\ConstructionParametres;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Datetime;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class EmprunteurController extends AbstractController
{
    /**
     * @Route("/assurance-pret-immobilier", name="app_emprunteur")
     * @param Request $request
     * @param ListeFormsRepository $repository
     * @param SalesforceController $salesforce
     * @param EntityManagerInterface $entityManager
     * @param ConstructionParametres $constructionParametres
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(
        Request $request,
        ListeFormsRepository $repository,
        SalesforceService $salesforce,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    )
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(11);
        $emprunteur = new Emprunteur();
        $form = $this->createForm(EmprunteurType::class, $emprunteur);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $emprunteur->setdate(new DateTime());
            $emprunteur->setAdresseIP($request->getClientIp());
            $emprunteur->setSourceClient('Action Payante Emprunteur');
            $emprunteur->setCodeCampagne($optionForm->getCampagne());
            $emprunteur->setProduit($optionForm->getNomFormulaire());
            $emprunteur->setUrlReferrer($referer);
            $emprunteur->setTracking($tracking);
            $salesforce->demandeActionApex($emprunteur);
            $entityManager->persist($emprunteur);
            $entityManager->flush();

            //$notification->notifyInscription($emprunteur->getEmail(), $emprunteur->getCivilite(), $emprunteur->getNom(), $emprunteur->getPrenom());

            return $this->redirectToRoute("app_emprunteur_confirmation");
        }
        return $this->render('emprunteur/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-assurance-pret-immobilier", name="app_emprunteur_confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(11);
        return $this->render('emprunteur/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("/admin/formulaire/assurance-pret-immobilier", name="admin_form_emprunteur")
     * @param ListeFormsRepository $listeFormsRepository
     * @param EmprunteurRepository $repository
     * @return RedirectResponse|Response
     */
    public function indexAdmin(ListeFormsRepository $listeFormsRepository, EmprunteurRepository $repository)
    {
        $leads = $repository->findAll();
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/formulaire/emprunteur/index.html.twig', [
            'Titre' => 'Formulaire Emprunteur',
            'leads' => $leads,
            'dataForms' => $dataForms
        ]);
    }
}
