<?php

namespace App\Controller;

use App\Controller\salesforce\SalesforceController;
use App\Entity\Auto;
use App\Form\AutoType;
use App\Repository\AutoRepository;
use App\Repository\ListeFormsRepository;
use App\Service\SalesforceService;
use App\Utils\ConstructionParametres;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AutoController extends AbstractController
{
    /**
     * @Route("/assurance-auto", name="app_auto")
     * @param Request $request
     * @param ListeFormsRepository $repository
     * @param SalesforceService $salesforce
     * @param EntityManagerInterface $entityManager
     * @param ConstructionParametres $constructionParametres
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(
        Request $request,
        ListeFormsRepository $repository,
        SalesforceService $salesforce,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    )
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(7);
        $auto = new Auto();
        $form = $this->createForm(AutoType::class, $auto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $auto->setdate(new DateTime());
            $auto->setAdresseIP($request->getClientIp());
            $auto->setSourceClient('Action Payante Auto');
            $auto->setCodeCampagne($optionForm->getCampagne());
            $auto->setProduit($optionForm->getNomFormulaire());
            $auto->setUrlReferrer($referer);
            $auto->setTracking($tracking);
            $salesforce->demandeActionApex($auto);
            $entityManager->persist($auto);
            $entityManager->flush();

            //$notification->notifyInscription($auto->getEmail(), $auto->getCivilite(), $auto->getNom(), $auto->getPrenom());

            return $this->redirectToRoute("app_auto_confirmation");
        }

        return $this->render('auto/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-assurance-auto", name="app_auto_confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(7);
        return $this->render('auto/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("/admin/formulaire/assurance-auto", name="admin_auto")
     * @param Request $request
     * @param ListeFormsRepository $listeFormsRepository
     * @param AutoRepository $repository
     * @return RedirectResponse|Response
     */
    public function indexAdmin(Request $request, ListeFormsRepository $listeFormsRepository, AutoRepository $repository)
    {
        $leads = $repository->findAll();
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/formulaire/auto/index.html.twig', [
            'Titre' => 'Formulaire Auto',
            'leads' => $leads,
            'dataForms' => $dataForms
        ]);
    }
}
