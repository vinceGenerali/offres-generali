<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Form\UserType;
use App\Repository\ListeFormsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller used to manage the application security.
 * See https://symfony.com/doc/current/security/form_login_setup.html.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("/admin/dashboard", name="admin_dashboard")
     * @param ListeFormsRepository $listeFormsRepository
     * @return Response
     */
    /*public function dashboardAdmin(ListeFormsRepository $listeFormsRepository)
    {
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/dashboard.html.twig', [
            'Titre' => 'Tableau de bord',
            'dataForms' => $dataForms
        ]);
    }*/

    /**
     * @Route("/admin/contact/nouveau", name="admin_contact_new")
     * @param ListeFormsRepository $listeFormsRepository
     * @return Response
     */
    public function contactAdmin(ListeFormsRepository $listeFormsRepository)
    {
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/mailbox/compose.html.twig', [
            'Titre' => 'Composer un message',
            'dataForms' => $dataForms
        ]);
    }

    /**
     * @Route("/admin/profil/edit", methods="GET|POST", name="admin_edit_profil")
     * @param Request $request
     * @param ListeFormsRepository $listeFormsRepository
     * @param UserPasswordHasherInterface $encoder
     * @return Response
     */
    public function editProfil(
        Request $request,
        ListeFormsRepository $listeFormsRepository,
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $encoder)
    {
        $dataForms = $listeFormsRepository->findAll();
        $user = $this->getUser();
        if (!$user) {
            $this->redirectToRoute('app_logout');
        }
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->hashPassword($user, $form->get('newPassword')->getData()));
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', 'user.updated_successfully');

            return $this->redirectToRoute('admin_edit_profil');
        }
        return $this->render('security/user/edit.html.twig', [
            'Titre' => 'Editer mon profil',
            'dataForms' => $dataForms,
            'form' => $form->createView(),
        ]);
    }

    /**
     * This is the route the user can use to logout.
     *
     * But, this will never be executed. Symfony will intercept this first
     * and handle the logout automatically. See logout in config/packages/security.yaml
     *
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \Exception('This should never be reached!');
    }
}
