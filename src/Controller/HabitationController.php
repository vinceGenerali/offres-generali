<?php

namespace App\Controller;

use App\Controller\salesforce\SalesforceController;
use App\Entity\Habitation;
use App\Form\HabitationType;
use App\Repository\HabitationRepository;
use App\Repository\ListeFormsRepository;
use App\Service\SalesforceService;
use App\Utils\ConstructionParametres;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class HabitationController extends AbstractController
{
    /**
     * @Route("/assurance-habitation", name="app_form_habitation")
     * @param Request $request
     * @param ListeFormsRepository $repository
     * @param SalesforceService $salesforce
     * @param EntityManagerInterface $entityManager
     * @param ConstructionParametres $constructionParametres
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(
        Request $request,
        ListeFormsRepository $repository,
        SalesforceService $salesforce,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    )
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(9);
        $habitation = new Habitation();
        $form = $this->createForm(HabitationType::class, $habitation);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $habitation->setdate(new DateTime());
            $habitation->setAdresseIP($request->getClientIp());
            $habitation->setSourceClient('Action Payante MRH');
            $habitation->setCodeCampagne($optionForm->getCampagne());
            $habitation->setProduit($optionForm->getNomFormulaire());
            $habitation->setUrlReferrer($referer);
            $habitation->setTracking($tracking);
            $salesforce->demandeActionApex($habitation);
            $entityManager->persist($habitation);
            $entityManager->flush();

            //$notification->notifyInscription($habitation->getEmail(), $habitation->getCivilite(), $habitation->getNom(), $habitation->getPrenom());

            return $this->redirectToRoute("confirmation");
        }

        return $this->render('habitation/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-assurance-habitation", name="confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(9);
        return $this->render('habitation/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("/admin/formulaire/assurance-habitation", name="admin_form_habitation")
     * @param HabitationRepository $repository
     * @param ListeFormsRepository $listeFormsRepository
     * @return RedirectResponse|Response
     */
    public function indexAdmin(HabitationRepository $repository, ListeFormsRepository $listeFormsRepository)
    {
        $leads = $repository->findAll();
        $dataForms = $listeFormsRepository->findAll();

        return $this->render('security/formulaire/assurance-habitation/index.html.twig', [
            'Titre' => 'Formulaire Habitation',
            'leads' => $leads,
            'dataForms' => $dataForms
        ]);
    }
}
