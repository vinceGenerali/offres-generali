<?php

namespace App\Controller;

use App\Controller\conquete\ConqueteController;
use App\Controller\conquete;
use App\Entity\Retraite;
use App\Form\RetraiteType;
use App\Repository\RetraiteRepository;
use App\Repository\ListeFormsRepository;
use App\Service\ConqueteService;
use App\Utils\ConstructionParametres;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class RetraiteController extends AbstractController
{
    /**
     * @Route("/contrat-retraite-per", name="app_retraite")
     * @param Request $request
     * @param ListeFormsRepository $repository
     * @param ConqueteService $conqueteService
     * @param EntityManagerInterface $entityManager
     * @param ConstructionParametres $constructionParametres
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(
        Request $request,
        ListeFormsRepository $repository,
        ConqueteService $conqueteService,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    )
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(5);
        $retraite = new Retraite();
        $form = $this->createForm(RetraiteType::class, $retraite);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $retraite->setdate(new DateTime());
            $retraite->setAdresseIP($request->getClientIp());
            $retraite->setSourceClient('Demande de rappel PRO');
            $retraite->setCodeCampagne($optionForm->getCampagne());
            $retraite->setProduit($optionForm->getNomProduit());
            $retraite->setCanal('');
            $retraite->setIdentifiantPartenaire(substr(uniqid('RET'),0, 10 ));
            $retraite->setUrlReferrer($referer);
            $retraite->setTracking($tracking);
            $conqueteLeadTechnicalId = $conqueteService->postDataAction($retraite);
            $retraite->setConqueteLeadTechnicalId($conqueteLeadTechnicalId);
            $entityManager->persist($retraite);
            $entityManager->flush();

            //$notification->notifyInscription($retraite->getEmail(), $retraite->getCivilite(), $retraite->getNom(), $retraite->getPrenom());

            return $this->redirectToRoute("app_retraite_confirmation");
        }

        return $this->render('retraite/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-contrat-retraite-per", name="app_retraite_confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(5);
        return $this->render('retraite/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("/admin/formulaire/contrat-retraite-per", name="admin_retraite")
     * @param ListeFormsRepository $listeFormsRepository
     * @param RetraiteRepository $repository
     * @return RedirectResponse|Response
     */
    public function indexAdmin(ListeFormsRepository $listeFormsRepository, RetraiteRepository $repository)
    {
        $leads = $repository->findAll();
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/formulaire/retraite/index.html.twig', [
            'Titre' => 'Formulaire Retraite Pro',
            'leads' => $leads,
            'dataForms' => $dataForms
        ]);
    }
}
