<?php

namespace App\Controller\ajax;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     * @Route("/ajax", name="ajax", methods="POST")
     * @param Request $request
     * @return Response
     */
    public function ajaxAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            // Ajax request
            return new Response('This is ajax response');
        } else {
            // Normal request
            return new Response('This is not ajax!', 400);
        }
    }
}
