<?php

namespace App\Controller;

use App\Controller\conquete\ConqueteController;
use App\Entity\MRC;
use App\Form\MRCType;
use App\Repository\ListeFormsRepository;
use App\Repository\MRCRepository;
use App\Service\ConqueteService;
use App\Utils\ConstructionParametres;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class MRCController extends AbstractController
{
    /**
     * @Route("/assurance-multirisque-professionnelle", name="app_form_mrc")
     * @param Request $request
     * @param ListeFormsRepository $repository
     * @param ConqueteService $conqueteService
     * @param EntityManagerInterface $entityManager
     * @param ConstructionParametres $constructionParametres
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(
        Request $request,
        ListeFormsRepository $repository,
        ConqueteService $conqueteService,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    )
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(2);
        $mrc = new MRC();
        $form = $this->createForm(MRCType::class, $mrc);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $mrc->setdate(new DateTime());
            $mrc->setAdresseIP($request->getClientIp());
            $mrc->setSourceClient('Demande de rappel PRO');
            $mrc->setCodeCampagne($optionForm->getCampagne());
            $mrc->setProduit($optionForm->getNomFormulaire());
            $mrc->setCanal('');
            $mrc->setIdentifiantPartenaire(substr(uniqid('MRC'),0, 10 ));
            $mrc->setUrlReferrer($referer);
            $mrc->setTracking($tracking);
            $conqueteLeadTechnicalId = $conqueteService->postDataAction($mrc);
            $mrc->setConqueteLeadTechnicalId($conqueteLeadTechnicalId);
            $entityManager->persist($mrc);
            $entityManager->flush();

            //$notification->notifyInscription($mrc->getEmail(), $mrc->getCivilite(), $mrc->getNom(), $mrc->getPrenom());

            return $this->redirectToRoute("app_form_mrc_confirmation");
        }

        return $this->render('mrc/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-assurance-multirisque-professionnelle", name="app_form_mrc_confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(2);
        return $this->render('mrc/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("/admin/formulaire/assurance-multirisque-professionnelle", name="admin_form_mrc")
     * @param ListeFormsRepository $listeFormsRepository
     * @param MRCRepository $repository
     * @return RedirectResponse|Response
     */
    public function indexAdmin(ListeFormsRepository $listeFormsRepository, MRCRepository $repository)
    {
        $leads = $repository->findAll();
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/formulaire/mrc/index.html.twig', [
            'Titre' => 'Formulaire MRC',
            'leads' => $leads,
            'dataForms' => $dataForms
        ]);
    }
}
