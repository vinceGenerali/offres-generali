<?php

namespace App\Controller;

use App\Controller\salesforce\SalesforceController;
use App\Entity\SanteSenior;
use App\Form\SanteSeniorType;
use App\Repository\ListeFormsRepository;
use App\Repository\SanteSeniorRepository;
use App\Service\SalesforceService;
use App\Utils\ConstructionParametres;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SanteSeniorController extends AbstractController
{
    /**
     * @Route("/complementaire-sante", name="app_sante_senior")
     * @param Request $request
     * @param ListeFormsRepository $repository
     * @param SalesforceService $salesforce
     * @param EntityManagerInterface $entityManager
     * @param ConstructionParametres $constructionParametres
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(
        Request $request,
        ListeFormsRepository $repository,
        SalesforceService $salesforce,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    )
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(10);
        $sante = new SanteSenior();
        $form = $this->createForm(SanteSeniorType::class, $sante);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $sante->setdate(new DateTime());
            $sante->setAdresseIP($request->getClientIp());
            $sante->setSourceClient('Action Payante Santé');
            $sante->setCodeCampagne($optionForm->getCampagne());
            $sante->setProduit($optionForm->getNomFormulaire());
            $sante->setUrlReferrer($referer);
            $sante->setTracking($tracking);
            $salesforce->demandeActionApex($sante);
            $entityManager->persist($sante);
            $entityManager->flush();

            //$notification->notifyInscription($sante->getEmail(), $sante->getCivilite(), $sante->getNom(), $sante->getPrenom());

            return $this->redirectToRoute("app_sante_senior_confirmation");
        }
        return $this->render('sante_senior/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-complementaire-sante", name="app_sante_senior_confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(10);
        return $this->render('sante_senior/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("/admin/formulaire/complementaire-sante", name="admin_sante_senior")
     * @param ListeFormsRepository $listeFormsRepository
     * @param SanteSeniorRepository $repository
     * @return RedirectResponse|Response
     */
    public function indexAdmin(ListeFormsRepository $listeFormsRepository, SanteSeniorRepository $repository)
    {
        $leads = $repository->findAll();
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/formulaire/sante_senior/index.html.twig', [
            'Titre' => 'Formulaire Santé Senior',
            'leads' => $leads,
            'dataForms' => $dataForms
        ]);
    }
}
