<?php

namespace App\Controller\Admin;

use App\Entity\ArgumentProduit;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ArgumentProduitCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ArgumentProduit::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
