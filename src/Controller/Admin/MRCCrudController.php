<?php

namespace App\Controller\Admin;

use App\Entity\MRC;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MRCCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return MRC::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
