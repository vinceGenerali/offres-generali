<?php

namespace App\Controller\Admin;

use App\Entity\Gpn;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class GpnCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Gpn::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
