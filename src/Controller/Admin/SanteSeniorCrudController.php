<?php

namespace App\Controller\Admin;

use App\Entity\SanteSenior;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SanteSeniorCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SanteSenior::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
