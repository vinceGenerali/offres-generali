<?php

namespace App\Controller\Admin;

use App\Entity\IdeoSante;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class IdeoSanteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return IdeoSante::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
