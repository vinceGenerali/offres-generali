<?php

namespace App\Controller\Admin;

use App\Entity\Emprunteur;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class EmprunteurCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Emprunteur::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
