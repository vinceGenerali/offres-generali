<?php

namespace App\Controller\Admin;

use App\Entity\Habitation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class HabitationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Habitation::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
