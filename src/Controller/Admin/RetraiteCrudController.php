<?php

namespace App\Controller\Admin;

use App\Entity\Retraite;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class RetraiteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Retraite::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
