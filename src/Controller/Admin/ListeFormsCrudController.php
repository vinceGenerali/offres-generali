<?php

namespace App\Controller\Admin;

use App\Entity\ListeForms;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ListeFormsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ListeForms::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
