<?php

namespace App\Controller\Admin;

use App\Entity\Prevoyance;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class PrevoyanceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Prevoyance::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
