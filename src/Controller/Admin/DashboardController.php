<?php

namespace App\Controller\Admin;

use App\Entity\Auto;
use App\Entity\ChienChat;
use App\Entity\Emprunteur;
use App\Entity\Gpn;
use App\Entity\Habitation;
use App\Entity\IdeoSante;
use App\Entity\ListeForms;
use App\Entity\MRC;
use App\Entity\Prevoyance;
use App\Entity\Retraite;
use App\Entity\SanteSenior;
use App\Entity\Spa;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin_")
 */
class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/tableau-de-bord", name="dashboard")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Plan Marque Generali');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Tableau de bord', 'fa fa-home'),
            MenuItem::section('Formulaires'),
            MenuItem::linkToCrud('Auto', 'fas fa-list', Auto::class),
            MenuItem::linkToCrud('Mon animal Generali', 'fas fa-list', ChienChat::class),
            MenuItem::linkToCrud('Emprunteur', 'fas fa-list', Emprunteur::class),
            MenuItem::linkToCrud('GPN', 'fas fa-list', Gpn::class),
            MenuItem::linkToCrud('Habitation', 'fas fa-list', Habitation::class),
            MenuItem::linkToCrud('Idéo Santé', 'fas fa-list', IdeoSante::class),
            MenuItem::linkToCrud('MRC', 'fas fa-list', MRC::class),
            MenuItem::linkToCrud('Prévoyance', 'fas fa-list', Prevoyance::class),
            MenuItem::linkToCrud('Retraite', 'fas fa-list', Retraite::class),
            MenuItem::linkToCrud('Santé Senior', 'fas fa-list', SanteSenior::class),
            MenuItem::linkToCrud('Mon animal Generali SPA', 'fas fa-list', Spa::class),
            MenuItem::section('Configuration générale'),
            MenuItem::linkToCrud('Mon animal Generali SPA', 'fas fa-list', ListeForms::class),
            MenuItem::section('Utilisateurs'),
            MenuItem::linkToCrud('Nouvel utilisateur', 'fas fa-plus', User::class)
                ->setAction('new')
                ->setPermission("ROLE_ADMIN"),
            MenuItem::linkToCrud('Utilisateurs', 'fas fa-user', User::class)
                ->setPermission("ROLE_ADMIN")
        ];
    }
}
