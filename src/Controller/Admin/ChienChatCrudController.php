<?php

namespace App\Controller\Admin;

use App\Entity\ChienChat;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ChienChatCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ChienChat::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
