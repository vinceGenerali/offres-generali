<?php

namespace App\Controller;

use App\Controller\salesforce\SalesforceController;
use App\Entity\Spa;
use App\Form\SpaType;
use App\Repository\ListeFormsRepository;
use App\Service\ConqueteService;
use App\Service\SalesforceService;
use App\Utils\ConstructionParametres;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class SpaController
 * @package App\Controller
 */
class SpaController extends AbstractController
{
    /**
     * @Route("spa-assurance-chien-chat", name="spa_home")
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @param ListeFormsRepository $repository
     * @param ConqueteService $conqueteService
     * @param EntityManagerInterface $entityManager
     * @param ConstructionParametres $constructionParametres
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(
        Request $request,
        ManagerRegistry $doctrine,
        ListeFormsRepository $repository,
        ConqueteService $conqueteService,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    ): Response
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(12);
        $spa = new Spa();
        $form = $this->createForm(SpaType::class, $spa);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $submittedToken = $request->request->get('token');
            if ($this->isCsrfTokenValid('delete-item', $submittedToken)) {
                $spa->setdate(new DateTime());
                $spa->setAdresseIP($request->getClientIp());
                $spa->setSourceClient('Demande de rappel AGENTS');
                $spa->setCodeCampagne($optionForm->getCampagne());
                $spa->setProduit($optionForm->getNomProduit());
                $spa->setIdentifiantPartenaire(substr(uniqid('SPA'),0, 10 ));
                $spa->setUrlReferrer($referer);
                $spa->setTracking($tracking);
                $conqueteLeadTechnicalId = $conqueteService->postDataAction($spa);
                $spa->setConqueteLeadTechnicalId($conqueteLeadTechnicalId);
                $entityManager->persist($spa);
                $entityManager->flush();
                return $this->redirectToRoute("spa_confirmation");
            }

        }
        return $this->render('spa/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-spa-assurance-chien-chat", name="spa_confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(
        ListeFormsRepository $repository
    ): Response
    {
        $optionForm = $repository->find(12);
        return $this->render('spa/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }
}
