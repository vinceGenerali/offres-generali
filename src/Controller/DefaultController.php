<?php


namespace App\Controller;

use App\Repository\ListeFormsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    public function index(): RedirectResponse
    {
        // redirects externally
        return $this->redirect('https://www.generali.fr/?utm_source=offres.generali.fr&utm_medium=referral&utm_campaign=fr_2020_11_performance_leads&zanpid=referral-offres.generali.fr-fr_2020_11_performance_leads');
    }

    /**
     * @Route("/mentions-legales", name="mentions")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function mentionsLegales(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(8);
        return $this->render('default/mentionsLegales.html.twig', [

        ]);
    }
}