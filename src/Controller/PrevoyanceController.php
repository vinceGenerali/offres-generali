<?php

namespace App\Controller;

use App\Controller\conquete\ConqueteController;
use App\Entity\Prevoyance;
use App\Form\PrevoyanceType;
use App\Repository\ListeFormsRepository;
use App\Repository\PrevoyanceRepository;
use App\Service\ConqueteService;
use App\Utils\ConstructionParametres;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class PrevoyanceController extends AbstractController
{
    /**
     * @Route("/assurance-prevoyance-pro", name="app_prevoyance")
     * @param Request $request
     * @param ListeFormsRepository $repository
     * @param ConqueteService $conqueteService
     * @param EntityManagerInterface $entityManager
     * @param ConstructionParametres $constructionParametres
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(
        Request $request,
        ListeFormsRepository $repository,
        ConqueteService $conqueteService,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    )
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(6);
        $prevoyance = new Prevoyance();
        $form = $this->createForm(PrevoyanceType::class, $prevoyance);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $prevoyance->setdate(new DateTime());
            $prevoyance->setAdresseIP($request->getClientIp());
            $prevoyance->setSourceClient('Demande de rappel PRO');
            $prevoyance->setCodeCampagne($optionForm->getCampagne());
            $prevoyance->setProduit($optionForm->getNomFormulaire());
            $prevoyance->setCanal('');
            $prevoyance->setIdentifiantPartenaire(substr(uniqid('PRV'),0, 10 ));
            $prevoyance->setUrlReferrer($referer);
            $prevoyance->setTracking($tracking);
            $conqueteLeadTechnicalId = $conqueteService->postDataAction($prevoyance);
            $prevoyance->setConqueteLeadTechnicalId($conqueteLeadTechnicalId);
            $entityManager->persist($prevoyance);
            $entityManager->flush();

            //$notification->notifyInscription($prevoyance->getEmail(), $prevoyance->getCivilite(), $prevoyance->getNom(), $prevoyance->getPrenom());

            return $this->redirectToRoute("app_prevoyance_confirmation");
        }

        return $this->render('prevoyance/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-assurance-prevoyance-pro", name="app_prevoyance_confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(6);
        return $this->render('prevoyance/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("/admin/formulaire/assurance-prevoyance-pro", name="admin_prevoyance")
     * @param ListeFormsRepository $listeFormsRepository
     * @param PrevoyanceRepository $repository
     * @return RedirectResponse|Response
     */
    public function indexAdmin(ListeFormsRepository $listeFormsRepository, PrevoyanceRepository $repository)
    {
        $leads = $repository->findAll();
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/formulaire/prevoyance/index.html.twig', [
            'Titre' => 'Formulaire Prévoyance Pro',
            'leads' => $leads,
            'dataForms' => $dataForms
        ]);
    }
}
