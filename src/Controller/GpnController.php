<?php

namespace App\Controller;

use App\Entity\Gpn;
use App\Form\GpnType;
use App\Repository\GpnRepository;
use App\Repository\ListeFormsRepository;
use App\Service\ConqueteService;
use App\Utils\ConstructionParametres;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class GpnController extends AbstractController
{
    /**
     * @Route("/assurance-cyber-risques", name="app_form_gpn")
     * @param Request $request
     * @param ListeFormsRepository $repository
     * @param ConqueteService $conqueteService
     * @param EntityManagerInterface $entityManager
     * @param ConstructionParametres $constructionParametres
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(
        Request $request,
        ListeFormsRepository $repository,
        ConqueteService $conqueteService,
        EntityManagerInterface $entityManager,
        ConstructionParametres $constructionParametres
    )
    {
        $referer = $constructionParametres->getReferer($request);
        $tracking = $constructionParametres->constructionTracking($request);
        $optionForm = $repository->find(1);
        $gpn = new Gpn();
        $form = $this->createForm(GpnType::class, $gpn);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $gpn->setDate(new DateTime('now'));
            $gpn->setAdresseIP($request->getClientIp());
            $gpn->setSourceClient('Demande de rappel PRO');
            $gpn->setCodeCampagne($optionForm->getCampagne());
            $gpn->setProduit($optionForm->getNomProduit());
            $gpn->setCanal('');
            $gpn->setIdentifiantPartenaire(substr(uniqid('GPN'),0, 10 ));
            $gpn->setUrlReferrer($referer);
            $gpn->setTracking($tracking);
            $conqueteLeadTechnicalId = $conqueteService->postDataAction($gpn);
            $gpn->setConqueteLeadTechnicalId($conqueteLeadTechnicalId);
            $entityManager->persist($gpn);
            $entityManager->flush();

            //$notification->notifyInscription($gpn->getEmail(), $gpn->getCivilite(), $gpn->getNom(), $gpn->getPrenom());

            return $this->redirectToRoute("app_form_gpn_confirmation");
        }

        return $this->render('gpn/index.html.twig', [
            'form' => $form->createView(),
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("merci-assurance-cyber-risques", name="app_form_gpn_confirmation")
     * @param ListeFormsRepository $repository
     * @return Response
     */
    public function confirmationParticipation(ListeFormsRepository $repository)
    {
        $optionForm = $repository->find(1);
        return $this->render('gpn/confirmation.html.twig', [
            'options' => $optionForm
        ]);
    }

    /**
     * @Route("/admin/formulaire/gpn", name="app_admin_form_gpn")
     * @param ListeFormsRepository $listeFormsRepository
     * @param GpnRepository $repository
     * @return RedirectResponse|Response
     */
    public function indexAdmin(ListeFormsRepository $listeFormsRepository, GpnRepository $repository)
    {
        $leads = $repository->findAll();
        $dataForms = $listeFormsRepository->findAll();
        return $this->render('security/formulaire/gpn/index.html.twig', [
            'Titre' => 'Formulaire GPN',
            'leads' => $leads,
            'dataForms' => $dataForms
        ]);
    }
}
