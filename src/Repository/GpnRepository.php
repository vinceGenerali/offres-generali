<?php

namespace App\Repository;

use App\Entity\Gpn;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Gpn|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gpn|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gpn[]    findAll()
 * @method Gpn[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GpnRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gpn::class);
    }

    // /**
    //  * @return Gpn[] Returns an array of Gpn objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Gpn
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
