<?php

namespace App\Repository;

use App\Entity\Prevoyance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Prevoyance|null find($id, $lockMode = null, $lockVersion = null)
 * @method Prevoyance|null findOneBy(array $criteria, array $orderBy = null)
 * @method Prevoyance[]    findAll()
 * @method Prevoyance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrevoyanceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Prevoyance::class);
    }

    // /**
    //  * @return Prevoyance[] Returns an array of Prevoyance objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Prevoyance
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
