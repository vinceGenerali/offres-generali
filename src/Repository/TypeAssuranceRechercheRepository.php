<?php

namespace App\Repository;

use App\Entity\TypeAssuranceRecherche;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeAssuranceRecherche|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeAssuranceRecherche|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeAssuranceRecherche[]    findAll()
 * @method TypeAssuranceRecherche[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeAssuranceRechercheRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeAssuranceRecherche::class);
    }

    // /**
    //  * @return TypeAssuranceRecherche[] Returns an array of TypeAssuranceRecherche objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeAssuranceRecherche
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
