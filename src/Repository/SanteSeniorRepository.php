<?php

namespace App\Repository;

use App\Entity\SanteSenior;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SanteSenior|null find($id, $lockMode = null, $lockVersion = null)
 * @method SanteSenior|null findOneBy(array $criteria, array $orderBy = null)
 * @method SanteSenior[]    findAll()
 * @method SanteSenior[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SanteSeniorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SanteSenior::class);
    }

    // /**
    //  * @return SanteSenior[] Returns an array of SanteSenior objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SanteSenior
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
