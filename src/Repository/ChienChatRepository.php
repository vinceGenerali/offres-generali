<?php

namespace App\Repository;

use App\Entity\ChienChat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ChienChat|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChienChat|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChienChat[]    findAll()
 * @method ChienChat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChienChatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChienChat::class);
    }

    // /**
    //  * @return ChienChat[] Returns an array of ChienChat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ChienChat
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
