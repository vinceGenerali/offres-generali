<?php

namespace App\Repository;

use App\Entity\RegimeSociale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RegimeSociale|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegimeSociale|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegimeSociale[]    findAll()
 * @method RegimeSociale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegimeSocialeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegimeSociale::class);
    }

    // /**
    //  * @return RegimeSociale[] Returns an array of RegimeSociale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegimeSociale
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
