<?php

namespace App\Repository;

use App\Entity\ArgumentProduit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ArgumentProduit|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArgumentProduit|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArgumentProduit[]    findAll()
 * @method ArgumentProduit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArgumentProduitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArgumentProduit::class);
    }

    // /**
    //  * @return ArgumentProduit[] Returns an array of ArgumentProduit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ArgumentProduit
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
