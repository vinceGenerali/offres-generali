<?php

namespace App\Repository;

use App\Entity\ListeForms;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ListeForms|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListeForms|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListeForms[]    findAll()
 * @method ListeForms[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListeFormsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListeForms::class);
    }

    // /**
    //  * @return ListeForms[] Returns an array of ListeForms objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ListeForms
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
