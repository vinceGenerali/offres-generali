<?php

namespace App\Repository;

use App\Entity\IdeoSante;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IdeoSante|null find($id, $lockMode = null, $lockVersion = null)
 * @method IdeoSante|null findOneBy(array $criteria, array $orderBy = null)
 * @method IdeoSante[]    findAll()
 * @method IdeoSante[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IdeoSanteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IdeoSante::class);
    }

    // /**
    //  * @return IdeoSante[] Returns an array of IdeoSante objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IdeoSante
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
